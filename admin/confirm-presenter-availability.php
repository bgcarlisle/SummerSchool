<?php

include_once ("../config.php");

include (ABS_PATH . "header.php");

$presenters = sch_get_presenters ("scheduled");
$confirmation_deadline = date("Y-m-d (D) H:i", min(strtotime(MOD_SIGNUP_START), strtotime(PARTIC_SIGNUP_START))) . " UTC";

?>
<div id="action-mask"></div>
<?php foreach ($presenters as $presenter) { ?>
    <div class="action-prompt" id="send-presenter-confirm-<?php echo $presenter['id']; ?>">
	<button class="btn btn-sm btn-secondary close-prompt" style="float: right;">Close</button>
	<div id="send-presenter-confirm-inner-<?php echo $presenter['id']; ?>">
	    <h4>Send confirmation email to <?php echo $presenter['name']; ?></h4>
	    <hr>
	    <p><strong>To:</strong> <?php echo $presenter['name']; ?> &lt;<?php echo $presenter['email']; ?>&gt;</p>
	    <p><strong>From:</strong> <?php echo CONF_NAME; ?> &lt;<?php echo SENDER_EMAIL; ?>&gt;</p>
	    <p><strong>Subject:</strong> Your presentation has been accepted; please confirm your presentation time</p>
	    <hr>
	    <p>Dear <?php echo $presenter['name']; ?>,</p>
	    <p>Thank you very much for submitting '<?php echo $presenter['title']; ?>' to <?php echo CONF_NAME; ?>.</p>
	    <p>We are pleased to accept your presentation and offer you the following presentation time:</p>
	    <p><?php echo date("Y-m-d (D) H:i", strtotime(CONF_START) + ($presenter['assigned_slot'] - 1) * 60 * 60); ?> UTC</p>
	    <p>To confirm that you are available for this time, please use the following link:</p>
	    <p><em>[Link will be inserted here]</em></p>
	    <p>We request that you confirm your presentation time as soon as possible so that we can recruit moderators and participants for your presentation.</p>
	    <p>Best regards,</p>
	    <p>The organization team<br><?php echo CONF_NAME; ?></p>
	    <hr>
	    <div class="alert alert-danger" role="alert">Warning: Sending an email cannot be undone</div>
	    <form action="<?php echo SITE_URL; ?>admin/confirm-presenter-availability.php" method="post">
		<input type="hidden" name="action" value="send">
		<input type="hidden" name="pid" value="<?php echo $presenter['id']; ?>">
		<button class="btn btn-lg btn-success send-presenter-confirm" data-presenter="<?php echo $presenter['id']; ?>">Send email</button>
		<button class="btn btn-lg btn-secondary close-prompt">Cancel</button>
	    </form>
	</div>
    </div>
<?php } ?>
<?php foreach ($presenters as $presenter) { ?>
    <div class="action-prompt" id="clear-presenter-confirm-<?php echo $presenter['id']; ?>">
	<button class="btn btn-sm btn-secondary close-prompt" style="float: right;">Close</button>
	<div id="clear-presenter-confirm-inner-<?php echo $presenter['id']; ?>">
	    <h4>Clear <?php echo $presenter['name']; ?></h4>
	    <p>Clicking "clear" below will deactivate any links that have been sent.</p>
	    <p>If the presenter has already confirmed their presentation time, this will also reset the presentation to "Not confirmed."</p>
	    <div class="alert alert-danger" role="alert">Warning: This cannot be undone</div>
	    <form action="<?php echo SITE_URL; ?>admin/confirm-presenter-availability.php" method="post">
		<input type="hidden" name="action" value="clear">
		<input type="hidden" name="pid" value="<?php echo $presenter['id']; ?>">
		<button class="btn btn-lg btn-danger clear-presenter-confirm" data-presenter="<?php echo $presenter['id']; ?>">Clear links and confirmation</button>
		<button class="btn btn-lg btn-secondary close-prompt">Cancel</button>
	    </form>
	</div>
    </div>
<?php } ?>
<div class="container">
    <div class="row">
	<div class="col-md-12">
	    <div class="five-second-removal">
	    <?php

	    if ($_POST['action'] == "send") {
		sch_send_presenter_confirmation_email ($_POST['pid']);
	    }

	    if ($_POST['action'] == "clear") {
		sch_clear_presenter_confirmation ($_POST['pid']);
		$presenters = sch_get_presenters ("accepted");
	    }

	    ?>
	    </div>
	    <nav aria-label="breadcrumb">
		<ol class="breadcrumb">
		    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>">Home</a></li>
		    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>admin/">Admin</a></li>
		    <li class="breadcrumb-item active" aria-current="page">Confirm presenter availability</li>
		</ol>
	    </nav>
	    <h3>Confirm presenter availability</h3>

	    <p>This form includes all accepted presentations that have been assigned a time using the scheduling page. The moderator signup form and the participant signup form will be populated using confirmed presentation times only.</p>
	    
	    <p><a href="<?php echo SITE_URL; ?>admin/csv-export.php">Download confirmed presentations as a .csv file</a></p>

	    <div class="table-responsive">
		<table class="table table-striped table-bordered table-sm">
		    <thead>
			<tr>
			    <td scope="col">
				Presentation
			    </td>
			    <td scope="col">
				Emailed links
			    </td>
			    <td scope="col">
				Status
			    </td>
			    <td scope="col" style="text-align: right;">
				Actions
			    </td>
			</tr>
		    </thead>
		    <tbody>
			<?php foreach ($presenters as $presenter) { ?>
			    <?php $presenter_links = sch_get_all_links_for_presenter ($presenter['id']); ?>
			    <?php $show_clear_button = FALSE; ?>
			    <tr>
				<td scope="row">
				    <?php echo $presenter['title']; ?>
				    <br>
				    <span class="text-muted"><?php echo $presenter['name']; ?></span>
				</td>
				<td>
				    <?php if ($presenter_links) { foreach ($presenter_links as $plink) { ?>
					<?php

					
					
					if ($plink['active'] == 1) {
					    $plink_class = "success";
					    $plink_alt = "The link that was sent to the presenter can still be used to confirm their presentation details";
					} else {
					    $plink_class = "secondary";
					    $plink_alt = "The link that was sent to the presenter has either been used or it has been cleared by an admin";
					}
					?>
					<span class="sent-link-to-presenter-<?php echo $presenter['id']; ?> badge bg-<?php echo $plink_class; ?>" title="<?php echo $plink_alt; ?>">
					    <?php
					    if ($plink['active'] == 1) {
						echo "Link is active";
						$show_clear_button = TRUE;
					    } else {
						echo "Link closed";
					    }
					    ?>
					</span>
				    <?php } } ?>
				</td>
				<td>
				    <?php

				    if (is_null($presenter['confirmed_slot'])) {
					$status_class = "secondary";
					$status_text = "Not confirmed";
					$status_alt = "The presenter has not yet confirmed their presentation time and details";
				    } else {
					$status_class = "success";
					$status_text = "Confirmed";
					$status_alt = "The presenter has confirmed their presentation time and details";
					$show_clear_button = TRUE;
				    }
				    
				    ?>
				    <span class="badge bg-<?php echo $status_class; ?>" title="<?php echo $status_alt; ?>">
					<?php echo $status_text; ?>
				    </span>
				</td>
				<td style="text-align: right;">
				    <button class="btn mb-2 btn-sm btn-primary prompt-send-presenter-confirm" data-presenter="<?php echo $presenter['id']; ?>">Send</button>

				    <?php if ($show_clear_button) { ?>
					<button id="prompt-clear-presenter-link-<?php echo $presenter['id']; ?>" class="btn mb-2 btn-sm btn-secondary prompt-clear-presenter-link" data-presenter="<?php echo $presenter['id']; ?>">Clear</button>
					<?php } ?>
				</td>
			    </tr>
			<?php } ?>
		    </tbody>
		</table>
	    </div>
	</div>
    </div>
</div>
<?php

include (ABS_PATH . "footer.php");

?>
