<?php

include_once ("../config.php");

include (ABS_PATH . "header.php");

?>
<div class="container">
    <div class="row">
	<div class="col-md-12">
	    <nav aria-label="breadcrumb">
		<ol class="breadcrumb">
		    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>">Home</a></li>
		    <li class="breadcrumb-item active" aria-current="page">Admin</li>
		</ol>
	    </nav>
	    <h3>Admin</h3>
	    <ul>
		<li><a href="<?php echo SITE_URL; ?>admin/review-presenter-applications.php">Review presenter applications</a></li>
		<li><a href="<?php echo SITE_URL; ?>admin/schedule-presentations.php">Schedule presentations</a></li>
		<li><a href="<?php echo SITE_URL; ?>admin/confirm-presenter-availability.php">Confirm presenter availability</a></li>
		<li><a href="<?php echo SITE_URL; ?>admin/review-moderators.php">Review moderators</a></li>
		<li><a href="<?php echo SITE_URL; ?>admin/match-moderators-to-presenters.php">Match moderators to presenters</a></li>
		<li><a href="<?php echo SITE_URL; ?>admin/confirm-moderation-details.php">Confirm moderation details</a></li>
		<li><a href="<?php echo SITE_URL; ?>admin/review-links.php">Review web conference room links and final schedule</a></li>
		<li><a href="<?php echo SITE_URL; ?>admin/introduce-presenters-moderators.php">Introduce presenters and moderators</a></li>
		<li><a href="<?php echo SITE_URL; ?>admin/invite-participants.php">Invite participants</a></li>
		<li><a href="<?php echo SITE_URL; ?>admin/presentation-materials.php">Presentation materials archive</a></li>
		<li><a href="<?php echo SITE_URL; ?>admin/view-feedback.php">View feedback</a></li>
	    </ul>
	</div>
    </div>
</div>
<?php

include (ABS_PATH . "footer.php");

?>
