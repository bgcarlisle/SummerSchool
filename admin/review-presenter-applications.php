<?php

include_once ("../config.php");

include (ABS_PATH . "header.php");

if ($_GET['show'] == "") {
    $_GET['show'] = "waiting";
}

$presenters = sch_get_presenters ($_GET['show']);

?>
<div class="container">
    <div class="row">
	<div class="col-md-12">
	    <nav aria-label="breadcrumb">
		<ol class="breadcrumb">
		    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>">Home</a></li>
		    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>admin/">Admin</a></li>
		    <li class="breadcrumb-item active" aria-current="page">Review presenter applications</li>
		</ol>
	    </nav>
	    <h3>Review presenter applications</h3>

	    <?php

	    switch ($_GET['show']) {
		case "all":
		    $show_all_button_disabled = ' disabled';
		    break;
		case "waiting":
		    $show_waiting_button_disabled = ' disabled';
		    break;
		case "accepted":
		    $show_accepted_button_disabled = ' disabled';
		    break;
		case "rejected":
		    $show_rejected_button_disabled = ' disabled';
		    break;
		case "withdrawn":
		    $show_withdrawn_button_disabled = ' disabled';
		    break;
		default:
		    $show_waiting_button_disabled = ' disabled';
		    break;
		    
	    }

	    ?>

	    <div class="btn-group mb-3" role="group" aria-label="Display by acceptance status">
		<a href="<?php echo SITE_URL; ?>admin/review-presenter-applications.php?show=waiting" role="button" class="btn btn-primary<?php echo $show_waiting_button_disabled; ?>">Waiting</a>
		<a href="<?php echo SITE_URL; ?>admin/review-presenter-applications.php?show=accepted" role="button" class="btn btn-primary<?php echo $show_accepted_button_disabled; ?>">Accepted</a>
		<a href="<?php echo SITE_URL; ?>admin/review-presenter-applications.php?show=rejected" role="button" class="btn btn-primary<?php echo $show_rejected_button_disabled; ?>">Rejected</a>
		<a href="<?php echo SITE_URL; ?>admin/review-presenter-applications.php?show=withdrawn" role="button" class="btn btn-primary<?php echo $show_withdrawn_button_disabled; ?>">Withdrawn</a>
		<a href="<?php echo SITE_URL; ?>admin/review-presenter-applications.php?show=all" role="button" class="btn btn-primary<?php echo $show_all_button_disabled; ?>">All</a>
	    </div>

	    <p><?php echo count($presenters); ?> <?php if ($_GET['show'] != "all") echo $_GET['show']; ?> presentation(s)</p>
	    
	    <?php

	    if (count ($presenters) > 0) {

		foreach ($presenters as $presenter) {

		    echo '<div class="card mb-3"><div class="card-body">';

		    switch ($presenter['presentation_accepted']) {
			case 0:
			    echo '<span id="status_indicator_' . $presenter['id'] . '" class="badge bg-secondary" style="float: right;">Waiting</span>';
			    break;
			case 1:
			    echo '<span id="status_indicator_' . $presenter['id'] . '" class="badge bg-success" style="float: right;">Accepted</span>';
			    break;
			case 2:
			    echo '<span id="status_indicator_' . $presenter['id'] . '" class="badge bg-danger" style="float: right;">Rejected</span>';
			    break;
			case 3:
			    echo '<span id="status_indicator_' . $presenter['id'] . '" class="badge bg-danger" style="float: right;">Withdrawn</span>';
			    break;
		    }

		    echo '<input type="hidden" id="presenter_status_' . $presenter['id'] . '" value="' . (int)$presenter['presentation_accepted'] . '">';
		    
		    echo '<h4 class="card-title">' . sch_format_text($presenter['title']) . '</h4>';

		    echo '<h5 class="text-muted">by ' . sch_format_text($presenter['name']) . '</h5>';

		    echo '<p class="text-muted">Email: ' . $presenter['email'] . '; Fediverse handle: ' . $presenter['handle'] . '</p>';

		    echo '<p>' . sch_format_text($presenter['abstract'], TRUE) . '</p>';

		    // Button group for updating status

		    echo '<hr><div class="form-group">';
		    
		    echo '<label for="status_select_' . $presenter['id'] . '">Application status</label>';
		    
		    echo '<div class="input-group mb-3">';

		    echo '<select class="form-control presenter_accept_status_select" data-presenter="' . $presenter['id'] . '" id="status_select_' . $presenter['id'] . '">';

		    switch ($presenter['presentation_accepted']) {
			case 0:
			    $waiting_selected = " selected";
			    break;
			case 1:
			    $accepted_selected = " selected";
			    break;
			case 2:
			    $rejected_selected = " selected";
			    break;
			case 3:
			    $withdrawn_selected = " selected";
			    break;
		    }
		    
		    echo '<option value="0"' . $waiting_selected . '>Waiting</option>';
		    echo '<option value="1"' . $accepted_selected . '>Accepted</option>';
		    echo '<option value="2"' . $rejected_selected . '>Rejected</option>';
		    echo '<option value="3"' . $withdrawn_selected . ' disabled>Withdrawn</option>';
		    
		    echo '</select>';
		    
		    echo '<button id="save_presenter_' . $presenter['id'] . '" class="btn btn-primary save_acceptance_status" type="button" data-presenter="' . $presenter['id'] . '" disabled>Save</button>';

		    echo '</div></div>';

		    // End button group

		    echo '</div></div>';
		    
		}
		
	    } else {

		echo '<p>There are currently no applications with this approval status.</p>';
		
	    }
	    
	    ?>
	</div>
    </div>
</div>
<?php

include (ABS_PATH . "footer.php");

?>
