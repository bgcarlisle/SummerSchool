<?php

include_once ("../config.php");

switch ($_GET['confirmed']) {
    case "yes":
	$presenters = sch_get_presenters ("confirmed-with-confirmed-mods");
	$filename = preg_replace("/[^a-zA-Z0-9]/", "_", CONF_NAME . "_Detailed_Schedule") . ".csv";
	$filename = preg_replace("/[_]+/", "_", $filename);
	break;
    case "no":
	$presenters = sch_get_presenters ("confirmed-with-matched-mods");
	$filename = preg_replace("/[^a-zA-Z0-9]/", "_", CONF_NAME . "_Tentative_Schedule") . ".csv";
	$filename = preg_replace("/[_]+/", "_", $filename);
	break;
    default:
	$presenters = sch_get_presenters ("confirmed-with-confirmed-mods");
	$filename = preg_replace("/[^a-zA-Z0-9]/", "_", CONF_NAME . "_Detailed_Schedule") . ".csv";
	$filename = preg_replace("/[_]+/", "_", $filename);
	break;
}

header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=' . $filename);

?>date,time,title,presenter_name,presenter_pronouns,presenter_handle,presenter_email,moderator_name,moderator_pronouns,moderator_handle,moderator_email,link
<?php

foreach ($presenters as $presenter) {

    $utc_date = date("Y-m-d", strtotime(CONF_START) + ($presenter['confirmed_slot'] - 1) * 60 * 60);
    $utc_time = date("H:i", strtotime(CONF_START) + ($presenter['confirmed_slot'] - 1) * 60 * 60);
    
    echo $utc_date . ",";
    echo $utc_time . ",";
    echo '"' . preg_replace("/[\"]+/", "'", $presenter['title']) . '",';
    
    echo '"' . preg_replace("/[\"]+/", "'", $presenter['name']) . '",';
    echo '"' . preg_replace("/[\"]+/", "'", $presenter['pronouns']) . '",';
    echo '"' . preg_replace("/[\"]+/", "'", $presenter['handle']) . '",';
    echo '"' . preg_replace("/[\"]+/", "'", $presenter['email']) . '",';
    
    echo '"' . preg_replace("/[\"]+/", "'", $presenter['moderators_name']) . '",';
    echo '"' . preg_replace("/[\"]+/", "'", $presenter['moderators_pronouns']) . '",';
    echo '"' . preg_replace("/[\"]+/", "'", $presenter['moderators_handle']) . '",';
    echo '"' . preg_replace("/[\"]+/", "'", $presenter['moderators_email']) . '",';
    
    echo '"' . preg_replace("/[\"]+/", "'", $presenter['hosting_details']) . '"';

    echo "\n";
    
}

?>
