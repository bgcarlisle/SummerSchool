<?php

include_once ("../config.php");

include (ABS_PATH . "header.php");

$presenters = sch_get_presenters ("confirmed");

?>
<div id="action-mask"></div>
<?php foreach ($presenters as $pre) { ?>
    <div class="action-prompt" id="send-presenter-request-<?php echo $pre['id']; ?>">
	<button class="btn btn-sm btn-secondary close-prompt" style="float: right;">Close</button>
	
	<h4>Request presentation materials for '<?php echo $pre['title']; ?>' by <?php echo $pre['name']; ?></h4>
	
	<p>Clicking the "Send" button below will email a link to the presenter, where they will be able to upload their presentation materials.</p>

	<hr>
	<p><strong>To:</strong> <?php echo $pre['name']; ?> &lt;<?php echo $pre['email']; ?>&gt;</p>

	<p><strong>From:</strong> <?php echo CONF_NAME; ?> &lt;<?php echo SENDER_EMAIL; ?>&gt;</p>

	<p><strong>Subject:</strong> Please upload the presentation materials for '<?php echo $pre['title']; ?>'</p>
	
	<hr>

	<p>Dear <?php echo $pre['name']; ?>,</p>

	<p>Thank you very much for your presentation at <?php echo CONF_NAME; ?>.</p>
	
	<p>We would like to offer to host your presentation materials, including notes, transcript, slides, recordings and any other relevant materials.</p>

	<?php if ($pre['provide_transcript']) { ?>

	    <p>You indicated at sign-up that you would be able to provide a transcript of your presentation. (Thank you!) If you are still willing, this is the place to do that!</p>

	    <?php } else { ?>

		<p>You indicated at sign-up that you would not be able to provide a transcript of your presentation. The option to upload materials and edit your presentation details is still available to you, but please do not feel any pressure to do so.</p>
		
	    <?php } ?>

	    <p>The page at the following link will allow you to confirm, edit or remove your presentation details, as well as upload any presentation materials that you would like us to host for you. There is no obligation to remain listed on this page.</p>

	    <p>[Link will be inserted here]</p>

	    <p>This link will expire after 72 hours, but you can always request a new link from the conference programme page:.</p>

	    <p><?php echo SITE_URL; ?>programme/</p>

	    <p>Any presentation materials submitted remain the property of the person who submits them. As much as is legally possible given the nature of this service, it is the policy of <?php echo CONF_NAME; ?> that users retain complete creative and legal control of their own submitted material. We will not sell or monetize your presentation materials in any way, and you can remove them at any time.</p>
	    <p>Best regards,</p>
	    <p>The organization team<br><?php echo CONF_NAME; ?></p>

	    <hr>
	    
	    <div class="alert alert-danger" role="alert">Warning: Sending an email cannot be undone</div>
	    <form action="<?php echo SITE_URL; ?>admin/presentation-materials.php" method="post">
		<input type="hidden" name="action" value="send">
		<input type="hidden" name="pid" value="<?php echo $pre['id']; ?>">
		<button class="btn btn-lg btn-success send-materials-request-confirm" data-presenter="<?php echo $pre['id']; ?>">Send email</button>
		<button class="btn btn-lg btn-secondary close-prompt">Cancel</button>
	    </form>


    </div>
<?php } ?>
<div class="container">
    <div class="row">
	<div class="col-md-12">
	    <div class="five-second-removal">
		<?php if ($_POST['action'] == "send") { if (sch_send_presenter_materials_request ($_POST['pid'])) { ?>
		    <div class="alert alert-success" role="alert">
			Email sent
		    </div>
		<?php } else { ?>
		    <div class="alert alert-danger" role="alert">
			Error; email not sent
		    </div>
		<?php } } ?>
	    </div>
	    
	    <nav aria-label="breadcrumb">
		<ol class="breadcrumb">
		    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>">Home</a></li>
		    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>admin/">Admin</a></li>
		    <li class="breadcrumb-item active" aria-current="page">Presentation materials archive</li>
		</ol>
	    </nav>
	    <h3>Presentation materials archive</h3>

	    <p>Each row of the following table lists one presentation per row. Clicking "Send" prompts you to send an email to the presenter, which will provide them with a link to a page where they can confirm, edit or remove their presentation details, as well as upload any relevant presentation materials. The presentation can also be manually removed using the drop-down menu in the "Hidden by admin" column.</p>
	    
	    <div class="table-responsive mb-3">
		<table class="table table-striped table-bordered table-sm">
		    <thead>
			<tr>
			    <td scope="col">
				Presentation
			    </td>
			    <td scope="col">
				Hidden by admin
			    </td>
			    <td scope="col">
				Hidden by presenter
			    </td>
			    <td scope="col">
				Emailed links
			    </td>
			    <td scope="col" style="text-align: right;">
				Request materials
			    </td>
			</tr>
		    </thead>
		    <tbody>
			<?php foreach ($presenters as $pre) { ?>
			    <tr>
				<td scope="row">
				    <h5><?php echo $pre['title'] ?></h5>
				    <p class="text-muted">by <?php echo $pre['name'] ?></p>
				</td>
				<td>
				    <div class="input-group mb-2">
					<select class="form-control presenter-materials-status-selector" id="presenter-materials-status-select-<?php echo $pre['id']; ?>" data-status="<?php echo $pre['hidden_from_archive_admin']; ?>" data-presenter="<?php echo $pre['id']; ?>">
					    <option value="0"<?php if ($pre['hidden_from_archive_admin'] == 0) { echo " selected"; } ?>>Visible on archive</option>
					    <option value="1"<?php if ($pre['hidden_from_archive_admin'] == 1) { echo " selected"; } ?>>Hidden from archive</option>
					</select>
					<button class="btn btn-primary save-presenter-materials-status" id="save-presenter-materials-status-<?php echo $pre['id']; ?>" type="button" data-presenter="<?php echo $pre['id']; ?>" disabled>Save</button>
				    </div>
				    <div class="alert" style="display: none;" role="alert" id="save-archive-hidden-feedback-<?php echo $pre['id']; ?>"></div>
				</td>
				<td>
				    <?php if ($pre['hidden_from_archive_presenter'] == 0) { ?>
					Not hidden by presenter
				    <?php } else { ?>
					Hidden by presenter
				    <?php } ?>
				</td>
				<td>
				    <?php $links = sch_get_archive_links_for_presentation ($pre['id']); ?>
				    <?php foreach ($links as $link) { ?>
					<?php if ($link['active'] == 1) { ?>
					    <span class="badge bg-success" title="A link was sent to the presenter on <?php echo substr($link['when_link_sent'], 0, 10); ?>">Link sent</span>
					<?php } else { ?>
					    <span class="badge bg-secondary" title="A link was sent to the presenter on <?php echo substr($link['when_link_sent'], 0, 10); ?>">Link sent</span>
					<?php } ?>
				    <?php } ?>
				</td>
				<td style="text-align: right;">
				    <button class="btn mb-2 btn-md btn-primary prompt-send-presenter-materials" data-presenter="<?php echo $pre['id']; ?>">Send</button>
				</td>
			    </tr>
			<?php } ?>
		    </tbody>
		</table>
	    </div>
			
	</div>
    </div>
</div>
<?php include (ABS_PATH . "footer.php"); ?>
