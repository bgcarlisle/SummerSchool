<?php

include_once ("../config.php");

include (ABS_PATH . "header.php");

$approved_presenters = sch_get_presenters ("accepted");
$availabilities = sch_all_approved_presenter_availabilities ();

?>
<div class="container">
    <div class="row">
	<div class="col-md-12">
	    <nav aria-label="breadcrumb">
		<ol class="breadcrumb">
		    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>">Home</a></li>
		    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>admin/">Admin</a></li>
		    <li class="breadcrumb-item active" aria-current="page">Schedule presentations</li>
		</ol>
	    </nav>
	    <h3>Schedule presentations</h3>
	    <?php

	    if (! $availabilities | ! $approved_presenters) {
		echo '<div class="alert alert-danger" role="alert">Error retrieving presenter availability</div>';
	    }
	    
	    ?>
	    <p>Aim to distribute the presentations evenly throughout the length of the conference. The bottom row of the grid for each day has a counter for the number of presentation in each 1-hour slot. If possible, don't schedule two presenters at the same time. Checkboxes only appear at times that the presenter in question indicated that they are available to present. When a box is checked, the remainder of the boxes for that presenter are disabled to prevent booking a presenter twice, and the "Status" column changes to "Scheduled" in the grid for every day, to make it easier to determine who still needs to be scheduled.</p>
	    <p>The schedule is not saved until you click "Save schedule" at the bottom. The schedule that is saved here is a private scheduler only, not the public schedule that is used for moderator and participant signups. Public-facing schedules are only updated after the presentation time has been confirmed by the presenter in the email link that is sent to them using the tool on the <a href="<?php echo SITE_URL; ?>admin/confirm-presenter-availability.php">Confirm presenter availability</a> page.</p>
	    <p>All times are UTC.</p>

	    <!-- NEW TABLE -->

	    <?php $grid_day = strtotime(CONF_START); ?>

	    <?php $day_slot = 1; ?>

	    <?php while ($grid_day < strtotime(CONF_END) + 86400) { ?>

		<h4><?php echo date("Y-m-d (D)", $grid_day); ?></h4>

		<div class="table-responsive mb-3">
		    <table class="table table-striped table-bordered table-sm presentation-sched">
			<thead>
			    <tr>
				<th scope="col">
				    Presenter
				</th>
				<th scope="col">
				    Status
				</th>
				<?php
				
				$grid_hour = 0;

				while ($grid_hour < 24) {

				    echo '<th scope="col">';
				    echo $grid_hour . ":00";
				    echo '</th>';
				    
				    $grid_hour++;
				}
				
				?>
			    </tr>
			</thead>
			<tbody>
			<?php foreach ($approved_presenters as $presenter) { ?>
			    <tr>
				<th scope="row"><?php echo $presenter['name']; ?></th>
				<?php

				if (is_null($presenter['assigned_slot'])) {
				    $sched_status_content = "Waiting";
				    $sched_status_class = "table-warning";
				} else {
				    $sched_status_content = "Scheduled";
				    $sched_status_class = "table-success";
				}
				
				?>
				<td class="presenter_sched_status_<?php echo $presenter['id']; ?> <?php echo $sched_status_class; ?>"><?php echo $sched_status_content; ?></td>
				<?php

				$grid_hour = 0;
				$presenter_slot = $day_slot;

				while ($grid_hour < 24) {

				    echo '<td class="sched-checkbox">';

				    if (sch_presenter_available($availabilities, $presenter['id'], $presenter_slot)) {
					if ($presenter['assigned_slot'] == $presenter_slot) {
					    $check_checkbox = " checked";
					} else {
					    $check_checkbox = "";
					}
					if (! is_null($presenter['assigned_slot']) & $presenter['assigned_slot'] != $presenter_slot) {
					    $disable_checkbox = " disabled";
					} else {
					    $disable_checkbox = "";
					}
					echo '<input class="form-check-input sched_slot_' . $presenter_slot . ' presenter_' . $presenter['id'] . '" data-presenter="' . $presenter['id'] . '" data-slot="' . $presenter_slot . '" type="checkbox"' . $disable_checkbox . $check_checkbox . '>';
				    }
				    
				    echo '</td>';
				    
				    $grid_hour++;
				    $presenter_slot++;
				}

				?>
			    </tr>
			<?php } ?>
			<tr>
			    <td colspan="2" class="table-secondary">Assigned</td>
			    <?php
			    
			    $grid_hour = 0;
			    $avail_slot = $day_slot;

			    while ($grid_hour < 24) {

				$taken = sch_slot_taken ($availabilities, $approved_presenters, $avail_slot);

				switch ($taken) {
				    case 0:
					$cell_class = ' table-secondary';
					$cell_contents = '&nbsp;';
					break;
				    case 1:
					$cell_class = ' table-success';
					$cell_contents = $taken;
					break;
				    case 2:
					$cell_class = ' table-warning';
					$cell_contents = $taken;
					break;
				    default:
					$cell_class = ' table-danger';
					$cell_contents = $taken;
					break;
				}
				
				echo '<td id="taken_' . $avail_slot . '" class="sched-checkbox' . $cell_class . '">' . $cell_contents . '</td>';
				
				$grid_hour++;
				$avail_slot++;
				$day_slot++;
			    }

			    ?>
			</tr>
			</tbody>
		    </table>
		</div>

		<?php $grid_day += 86400; ?>

	    <?php } ?>

	    <input type="hidden" id="schedule_selections">
	    <button class="btn btn-primary btn-lg" id="save_schedule_button" disabled="true">Save schedule</button>
	    <div id="schedule_save_feedback" class="alert mt-3" role="alert" style="display: none;"></div>
	</div>
    </div>
</div>
<?php

include (ABS_PATH . "footer.php");

?>
