<?php

include_once ("../config.php");

include (ABS_PATH . "header.php");

$presenters = sch_get_presenters ("confirmed-with-confirmed-mods");

$participants = sch_get_participants ();

?>
<div id="action-mask"></div>
<?php foreach ($presenters as $pre) { ?>
    <div class="action-prompt" id="send-participant-invite-<?php echo $pre['id']; ?>">
	<button class="btn btn-sm btn-secondary close-prompt" style="float: right;">Close</button>

	<h4>Invite participants to '<?php echo $pre['title']; ?>' by <?php echo $pre['name']; ?></h4>

	<p class="text-muted">
	    <?php

	    $utc_time = date("Y-m-d (D) H:i", strtotime(CONF_START) + ($pre['confirmed_slot'] - 1) * 60 * 60) . " UTC";

	    echo $utc_time;
	
	    ?>
	</p>

	<p>Clicking the "Send to approved" button below will email all the participants on the list whose status is "Accepted".</p>

	<h5>Participants</h5>
	<table class="table table-striped table-bordered table-sm">
	    <thead>
		<td scope="col">
		    Email
		</td>
		<td scope="col">
		    Handle
		</td>
		<td scope="col">
		    Participant status
		</td>
	    </thead>
	    <tbody>
		<?php foreach ($participants as $part) { if ($part['presenter'] == $pre['id']) { ?>
		    <tr>
			<td><?php echo $part['email']; ?></td>
			<td><?php echo $part['handle']; ?></td>
			<td>
			    <div class="input-group mb-2">
				<select class="form-control participant-signup-status-selector" id="participant-signup-status-select-<?php echo $part['id']; ?>">
				    <option value="1"<?php if ($part['accepted'] == 1) { echo " selected"; } ?>>Accepted</option>
				    <option value="0"<?php if ($part['accepted'] == 0) { echo " selected"; } ?>>Rejected</option>
				</select>
				<button class="btn btn-primary save-participant-signup-status" type="button" data-participant="<?php echo $part['id']; ?>" disabled>Save</button>
			    </div>
			    <div class="alert" style="display: none;" role="alert" id="participant-save-status-feedback-<?php echo $part['id']; ?>"></div>
			</td>
		    </tr>
		<?php } } ?>
	    </tbody>
	</table>

	<hr>

	<p><strong>From:</strong> <?php echo CONF_NAME; ?> &lt;<?php echo SENDER_EMAIL; ?>&gt;</p>

	<p><strong>Subject:</strong> Details for <?php echo CONF_NAME; ?> presentation: <?php echo $pre['title']; ?></p>
	
	<hr>

	<p>Thank you for your interest in attending '<?php echo $pre['title']; ?>' by <?php echo $pre['name']; ?> at <?php echo CONF_NAME; ?>.</p>

	<p>This presentation will begin at: <?php echo $utc_time; ?>.</p>

	<p>You can join the presentation at the following link:</p>

	<p><?php echo $pre['hosting_details']; ?></p>

	<p>Please make every effort to arrive on time, as the presentation will be only 15 minutes long.</p>

	<p>You can familiarize yourself with the rules for participants at the following address:</p>

	<p><?php echo SITE_URL ?>participants/</p>

	<p>If you have any questions, please contact us: <?php echo SITE_URL; ?>contact/</p>

	<p>Best regards,</p>
	<p>The organization team<br><?php echo CONF_NAME; ?></p>

	<hr>

	<div class="alert alert-danger" role="alert">
	    Warning: Sending an email cannot be undone
	</div>
	<form action="<?php echo SITE_URL; ?>admin/invite-participants.php" method="post">
	    <input type="hidden" name="action" value="send-all">
	    <input type="hidden" name="presenter" value="<?php echo $pre['id']; ?>">
	    <button class="btn btn-success btn-lg">Send to approved</button>
	    <button class="btn btn-secondary btn-lg close-prompt">Cancel</button>
	</form>
    </div>
<?php } ?>
<div class="container">
    <div class="row">
	<div class="col-md-12">
	    <div class="five-second-removal">
		<?php

		if ($_POST['action'] == "send-all") {
		    if (sch_send_participant_invites ($_POST['presenter'])) {
			echo '<div class="alert alert-success" role="alert">Email sent!</div>';
		    } else {
			echo '<div class="alert alert-danger" role="alert">Error sending email</div>';
		    }
		}

		$sent_invites = sch_get_sent_invite_emails ();
		
		?>
	    </div>
	    <nav aria-label="breadcrumb">
		<ol class="breadcrumb">
		    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>">Home</a></li>
		    <li class="breadcrumb-item"><a href="<?php echo SITE_URL; ?>admin/">Admin</a></li>
		    <li class="breadcrumb-item active" aria-current="page">Invite participants</li>
		</ol>
	    </nav>
	    <h3>Invite participants</h3>

	    
	    <div class="table-responsive mb-3">
		<table class="table table-striped table-sm">
		    <thead>
			<tr>
			    <td scope="col">
				Presentation
			    </td>
			    <td scope="col" style="text-align: right;">
				Actions
			    </td>
			</tr>
		    </thead>
		    <tbody>
			<?php foreach ($presenters as $pre) { ?>
			    <tr>
				<td>
				    <h4><?php echo $pre['title']; ?></h4>
				    <p class="text-muted">by <?php echo $pre['name']; ?></p>
				    <?php foreach ($sent_invites as $si) { if ($si['presenter'] == $pre['id']) { ?>
					<span class="badge bg-success">Email sent</span>
				    <?php } } ?>
				</td>
				<td style="text-align: right;">
				    <button class="btn btn-sm btn-primary prompt-email-participants-button" data-presenter="<?php echo $pre['id']; ?>">Email</button>
				</td>
			    </tr>
			<?php } ?>
		    </tbody>
		</table>
	    </div>

	    
	</div>
    </div>
</div>
<?php include (ABS_PATH . "footer.php"); ?>
