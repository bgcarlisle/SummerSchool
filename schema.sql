-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: mysql.bgcarlisle.com
-- Generation Time: Oct 28, 2022 at 10:18 AM
-- Server version: 8.0.29-0ubuntu0.20.04.3
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sumschool`
--

-- --------------------------------------------------------

--
-- Table structure for table `archive_link`
--

CREATE TABLE `archive_link` (
  `id` int NOT NULL,
  `presenter` int NOT NULL,
  `link` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `active` int NOT NULL DEFAULT '1',
  `when_link_sent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `archive_materials`
--

CREATE TABLE `archive_materials` (
  `id` int NOT NULL,
  `when_uploaded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sortorder` int NOT NULL,
  `presenter` int NOT NULL,
  `materials_type` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `materials_title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci,
  `materials_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `id` int NOT NULL,
  `when_sent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `link` varchar(12) COLLATE utf8mb4_general_ci NOT NULL,
  `contact` text COLLATE utf8mb4_general_ci NOT NULL,
  `comments` text COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `feedback_link`
--

CREATE TABLE `feedback_link` (
  `id` int NOT NULL,
  `presenter` int NOT NULL,
  `role` text COLLATE utf8mb4_general_ci NOT NULL,
  `link` varchar(12) COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `intro_emails_sent`
--

CREATE TABLE `intro_emails_sent` (
  `id` int NOT NULL,
  `presenter` int NOT NULL,
  `when_sent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `role` text COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `invite_emails_sent`
--

CREATE TABLE `invite_emails_sent` (
  `id` int NOT NULL,
  `presenter` int NOT NULL,
  `when_sent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `moderators`
--

CREATE TABLE `moderators` (
  `id` int NOT NULL,
  `name` text COLLATE utf8mb4_general_ci NOT NULL,
  `pronouns` text COLLATE utf8mb4_general_ci NOT NULL,
  `handle` text COLLATE utf8mb4_general_ci NOT NULL,
  `email` text COLLATE utf8mb4_general_ci NOT NULL,
  `max_mods` int NOT NULL,
  `able_to_host` int NOT NULL,
  `moderator_accepted` int NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `moderator_availability`
--

CREATE TABLE `moderator_availability` (
  `id` int NOT NULL,
  `moderator` int NOT NULL,
  `presentation` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `moderator_confirmation`
--

CREATE TABLE `moderator_confirmation` (
  `id` int NOT NULL,
  `when_sent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `moderator` int NOT NULL,
  `link` varchar(12) COLLATE utf8mb4_general_ci NOT NULL,
  `active` int NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `participants`
--

CREATE TABLE `participants` (
  `id` int NOT NULL,
  `email` text COLLATE utf8mb4_general_ci NOT NULL,
  `handle` text COLLATE utf8mb4_general_ci NOT NULL,
  `link` varchar(12) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `when_link_sent` timestamp NULL DEFAULT NULL,
  `email_verified` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `participant_signups`
--

CREATE TABLE `participant_signups` (
  `id` int NOT NULL,
  `participant` int NOT NULL,
  `presenter` int NOT NULL,
  `accepted` int NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `presenters`
--

CREATE TABLE `presenters` (
  `id` int NOT NULL,
  `signup_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `name` text COLLATE utf8mb4_general_ci NOT NULL,
  `pronouns` text COLLATE utf8mb4_general_ci NOT NULL,
  `handle` text COLLATE utf8mb4_general_ci NOT NULL,
  `email` text COLLATE utf8mb4_general_ci NOT NULL,
  `title` text COLLATE utf8mb4_general_ci NOT NULL,
  `abstract` text COLLATE utf8mb4_general_ci NOT NULL,
  `able_to_host` int NOT NULL,
  `hosting_details` text COLLATE utf8mb4_general_ci NOT NULL,
  `consent_to_record` int NOT NULL,
  `provide_transcript` int NOT NULL,
  `presentation_accepted` int DEFAULT NULL,
  `assigned_slot` int DEFAULT NULL,
  `confirmed_slot` int DEFAULT NULL,
  `matched_mod` int DEFAULT NULL,
  `confirmed_mod` int DEFAULT NULL,
  `hidden_from_archive_admin` int NOT NULL DEFAULT '0',
  `hidden_from_archive_presenter` int NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `presenter_availability`
--

CREATE TABLE `presenter_availability` (
  `id` int NOT NULL,
  `presenter` int NOT NULL,
  `slot` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `presenter_confirmation`
--

CREATE TABLE `presenter_confirmation` (
  `id` int NOT NULL,
  `when_sent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `presenter` int NOT NULL,
  `slot` int NOT NULL,
  `link` varchar(12) COLLATE utf8mb4_general_ci NOT NULL,
  `active` int NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int NOT NULL,
  `setting` text COLLATE utf8mb4_general_ci NOT NULL,
  `value` text COLLATE utf8mb4_general_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `archive_link`
--
ALTER TABLE `archive_link`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `archive_materials`
--
ALTER TABLE `archive_materials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback_link`
--
ALTER TABLE `feedback_link`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `intro_emails_sent`
--
ALTER TABLE `intro_emails_sent`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `invite_emails_sent`
--
ALTER TABLE `invite_emails_sent`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `moderators`
--
ALTER TABLE `moderators`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `moderator_availability`
--
ALTER TABLE `moderator_availability`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `moderator_confirmation`
--
ALTER TABLE `moderator_confirmation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `participants`
--
ALTER TABLE `participants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `participant_signups`
--
ALTER TABLE `participant_signups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `presenters`
--
ALTER TABLE `presenters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `presenter_availability`
--
ALTER TABLE `presenter_availability`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `presenter_confirmation`
--
ALTER TABLE `presenter_confirmation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `archive_link`
--
ALTER TABLE `archive_link`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `archive_materials`
--
ALTER TABLE `archive_materials`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `feedback_link`
--
ALTER TABLE `feedback_link`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `intro_emails_sent`
--
ALTER TABLE `intro_emails_sent`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `invite_emails_sent`
--
ALTER TABLE `invite_emails_sent`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `moderators`
--
ALTER TABLE `moderators`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `moderator_availability`
--
ALTER TABLE `moderator_availability`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `moderator_confirmation`
--
ALTER TABLE `moderator_confirmation`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `participants`
--
ALTER TABLE `participants`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `participant_signups`
--
ALTER TABLE `participant_signups`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `presenters`
--
ALTER TABLE `presenters`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `presenter_availability`
--
ALTER TABLE `presenter_availability`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `presenter_confirmation`
--
ALTER TABLE `presenter_confirmation`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
