<?php

session_start();

date_default_timezone_set("UTC");

// Read in 
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require PHP_MAILER_PATH . 'PHPMailer/src/Exception.php';
require PHP_MAILER_PATH . 'PHPMailer/src/PHPMailer.php';
require PHP_MAILER_PATH . 'PHPMailer/src/SMTP.php';

function sch_email_organizers ($email, $message) {

    $subject = "Message submitted through website " . date("Y-m-d H:i") . " (UTC)";

    $body = "Email: " . $email . "\n\nMessage:\n\n" . $message;

    if (sch_email(CONTACT_EMAIL, $subject, $body)) {
	echo "<p>Message sent</p>";
    } else {
	echo "<p>Error: Message not sent</p>";
    }
    
}

function sch_email ($to, $subject, $body) {

    $mail = new PHPMailer(true);

    try {
	// Server settings
	// $mail->SMTPDebug = 2;
	$mail->isSMTP();
	$mail->Host = SMTP_HOST;
	$mail->SMTPAuth = true;
	$mail->Username = SENDER_EMAIL;
	$mail->Password = SENDER_PASS;
	$mail->SMTPSecure = 'ssl';
	$mail->Port = SMTP_PORT;

	// Email headers
	$mail->setFrom(SENDER_EMAIL, CONF_NAME);
	$mail->addAddress($to);
	$mail->addReplyTo(SENDER_EMAIL, CONF_NAME);

	// Email content
	$mail->isHTML(false);
	$mail->Subject = $subject;
	$mail->Body = $body;

	// Send email
	$mail->send();
	return TRUE;
	
    } catch (Exception $e) {
	return FALSE;
    }
    
}

function sch_email_many ($to, $subject, $body) {

    $mail = new PHPMailer(true);
    
    try {
	// Server settings
	// $mail->SMTPDebug = 2;
	$mail->isSMTP();
	$mail->Host = SMTP_HOST;
	$mail->SMTPAuth = true;
	$mail->Username = SENDER_EMAIL;
	$mail->Password = SENDER_PASS;
	$mail->SMTPSecure = 'ssl';
	$mail->Port = SMTP_PORT;

	// Email headers
	$mail->setFrom(SENDER_EMAIL, CONF_NAME);
	$mail->addReplyTo(SENDER_EMAIL, CONF_NAME);
	foreach ($to as $recip_address) {
	    $mail->addBCC($recip_address);
	}
	$mail->addAddress(SENDER_EMAIL);

	// Email content
	$mail->isHTML(false);
	$mail->Subject = $subject;
	$mail->Body = $body;

	// Send email
	$mail->send();
	return TRUE;
	
    } catch (Exception $e) {
	return FALSE;
    }
    
}

function sch_save_presenter ($name, $pronouns, $handle, $email, $title, $abstract, $host, $recording, $transcript, $availability) {

    if (preg_match("/^([0-9]+[0-9,]+[0-9]+|[0-9]+)$/", $availability)) {
	
	try {
	    
	    $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	    $stmt = $dbh->prepare("INSERT INTO `presenters` (`name`, `pronouns`, `handle`, `email`, `title`, `abstract`, `able_to_host`, `consent_to_record`, `provide_transcript`) VALUES (:name, :pronouns, :handle, :email, :title, :abstract, :host, :record, :transcript);");

	    $stmt->bindParam(':name', $na);
	    $stmt->bindParam(':pronouns', $pr);
	    $stmt->bindParam(':handle', $ha);
	    $stmt->bindParam(':email', $em);
	    $stmt->bindParam(':title', $ti);
	    $stmt->bindParam(':abstract', $ab);
	    $stmt->bindParam(':host', $ho);
	    $stmt->bindParam(':record', $re);
	    $stmt->bindParam(':transcript', $tr);

	    $na = $name;
	    $pr = $pronouns;
	    $ha = $handle;
	    $em = $email;
	    $ti = $title;
	    $ab = $abstract;
	    $ho = $host;
	    $re = $recording;
	    $tr = $transcript;

	    if ($stmt->execute()) {
		$details_saved = TRUE;
		
		$stmt2 = $dbh->prepare("SELECT LAST_INSERT_ID() AS newid;");
		$stmt2->execute();

		$result = $stmt2->fetchAll();

		$newid = $result[0]['newid'];
		
	    } else {
		$details_saved = FALSE;
		echo "<p>Error saving presenter details. Please contact the organizers.</p>";
	    }

	}

	catch (PDOException $e) {

	    echo $e->getMessage();

	}

	if ($details_saved) {

	    $slots = explode(",", $availability);

	    $avail_query = "INSERT INTO `presenter_availability`(presenter, slot) VALUES ";

	    $slots_queries = [];
	    
	    foreach ($slots as $slot) {
		array_push($slots_queries, "(" . $newid . "," . $slot . ")");
	    }

	    $avail_query .= implode(", ", $slots_queries);

	    try {
		
		$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
		$stmt = $dbh->prepare($avail_query);

		if ($stmt->execute()) {
		    echo "<p>Presenter application has been successfully sent!</p>";
		} else {
		    echo "<p>Error sending presenter application. Please contact the organizers.</p>";
		}

	    }

	    catch (PDOException $e) {

		echo $e->getMessage();

	    }
	    
	    
	}

    } else {
	echo "<p>Availability input is not well-formed</p>";
    }
    
}

function sch_save_moderator ($name, $pronouns, $handle, $email, $max_mods, $host, $availability) {

    if (preg_match("/^([0-9]+[0-9,]+[0-9]+|[0-9]+)$/", $availability)) {

	try {
	    
	    $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	    $stmt = $dbh->prepare("INSERT INTO `moderators` (`name`, `pronouns`, `handle`, `email`, `max_mods`, `able_to_host`) VALUES (:name, :pronouns, :handle, :email, :max_mods, :host);");

	    $stmt->bindParam(':name', $na);
	    $stmt->bindParam(':pronouns', $pr);
	    $stmt->bindParam(':handle', $ha);
	    $stmt->bindParam(':email', $em);
	    $stmt->bindParam(':max_mods', $mm);
	    $stmt->bindParam(':host', $ho);

	    $na = $name;
	    $pr = $pronouns;
	    $ha = $handle;
	    $em = $email;
	    $mm = $max_mods;
	    $ho = $host;

	    if ($stmt->execute()) {
		$details_saved = TRUE;
		
		$stmt2 = $dbh->prepare("SELECT LAST_INSERT_ID() AS newid;");
		$stmt2->execute();

		$result = $stmt2->fetchAll();

		$newid = $result[0]['newid'];
		
	    } else {
		$details_saved = FALSE;
		echo "<p>Error saving moderator details. Please contact the organizers.</p>";
	    }

	}

	catch (PDOException $e) {

	    echo $e->getMessage();

	}
	
    }

    if ($details_saved) {

	$presentations = explode(",", $availability);

	$avail_query = "INSERT INTO `moderator_availability`(moderator, presentation) VALUES ";

	$mods_queries = [];
	
	foreach ($presentations as $presentation) {
	    array_push($mods_queries, "(" . $newid . "," . $presentation . ")");
	}

	$avail_query .= implode(", ", $mods_queries);

	try {
	    
	    $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	    $stmt = $dbh->prepare($avail_query);

	    if ($stmt->execute()) {
		echo "<p>Moderator application has been successfully sent! We will contact you shortly to let you know which presentation(s) you will be moderating.</p>";
	    } else {
		echo "<p>Error sending moderator application. Please contact the organizers.</p>";
	    }

	}

	catch (PDOException $e) {

	    echo $e->getMessage();

	}
	
    }
    
}

function sch_get_presenters ($show = "all") {

    switch ($show) {
	case "all":
	    $query = "SELECT * FROM `presenters`";
	    break;
	case "waiting":
	    $query = "SELECT * FROM `presenters` WHERE `presentation_accepted` IS NULL OR `presentation_accepted` = 0;";
	    break;
	case "accepted":
	    $query = "SELECT * FROM `presenters` WHERE `presentation_accepted` = 1;";
	    break;
	case "scheduled":
	    $query = "SELECT * FROM `presenters` WHERE `presentation_accepted` = 1 AND `assigned_slot` IS NOT NULL;";
	    break;
	case "confirmed":
	    $query = "SELECT * FROM `presenters` WHERE `presentation_accepted` = 1 AND `assigned_slot` IS NOT NULL AND `confirmed_slot` IS NOT NULL ORDER BY `confirmed_slot` ASC;";
	    break;
	case "confirmed-with-matched-mods":
	    $query = "SELECT `presenters`.`id` AS `id`, `presenters`.`name` AS `name`, `presenters`.`pronouns` AS `pronouns`, `presenters`.`handle` AS `handle`, `presenters`.`email` AS `email`, `presenters`.`title` AS `title`, `presenters`.`able_to_host` AS `able_to_host`, `presenters`.`hosting_details` AS `hosting_details`, `presenters`.`confirmed_slot` AS `confirmed_slot`, `presenters`.`confirmed_mod` AS `confirmed_mod`, `moderators`.`id` AS `moderators_id`, `moderators`.`name` AS `moderators_name`, `moderators`.`pronouns` AS `moderators_pronouns`, `moderators`.`handle` AS `moderators_handle`, `moderators`.`email` AS `moderators_email`, `moderators`.`able_to_host` as `moderators_able_to_host` FROM `presenters`, `moderators` WHERE `presenters`.`matched_mod` = `moderators`.`id` AND `presenters`.`presentation_accepted` = 1 AND `presenters`.`assigned_slot` IS NOT NULL AND `presenters`.`confirmed_slot` IS NOT NULL ORDER BY `presenters`.`confirmed_slot` ASC;";
	    break;
	case "confirmed-with-confirmed-mods":
	    $query = "SELECT `presenters`.`id` AS `id`, `presenters`.`name` AS `name`, `presenters`.`pronouns` AS `pronouns`, `presenters`.`handle` AS `handle`, `presenters`.`email` AS `email`, `presenters`.`title` AS `title`, `presenters`.`able_to_host` AS `able_to_host`, `presenters`.`hosting_details` AS `hosting_details`, `presenters`.`confirmed_slot` AS `confirmed_slot`, `presenters`.`confirmed_mod` AS `confirmed_mod`, `moderators`.`id` AS `moderators_id`, `moderators`.`name` AS `moderators_name`, `moderators`.`pronouns` AS `moderators_pronouns`, `moderators`.`handle` AS `moderators_handle`, `moderators`.`email` AS `moderators_email`, `moderators`.`able_to_host` as `moderators_able_to_host`, `presenters`.`hidden_from_archive_admin` AS `hidden_from_archive_admin`, `presenters`.`hidden_from_archive_presenter` AS `hidden_from_archive_presenter` FROM `presenters`, `moderators` WHERE `presenters`.`confirmed_mod` = `moderators`.`id` AND `presenters`.`presentation_accepted` = 1 AND `presenters`.`assigned_slot` IS NOT NULL AND `presenters`.`confirmed_slot` IS NOT NULL ORDER BY `presenters`.`confirmed_slot` ASC;";
	    break;
	case "rejected":
	    $query = "SELECT * FROM `presenters` WHERE `presentation_accepted` = 2;";
	    break;
	case "withdrawn":
	    $query = "SELECT * FROM `presenters` WHERE `presentation_accepted` = 3;";
	    break;
    }

    try {
	
	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	$stmt = $dbh->prepare($query);

	if ($stmt->execute()) {
	    $result = $stmt->fetchAll();
	    return $result;
	} else {
	    return FALSE;
	}

    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function sch_get_presenter ($presenter) {
    // This function only returns one row from the `presenters` table
    // $presenter is the `id` column for the presenter in question

    try {
	
	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	$stmt = $dbh->prepare("SELECT * FROM `presenters` WHERE `id` = :pid LIMIT 1;");
	
	$stmt->bindParam(':pid', $pid);

	$pid = $presenter;

	if ($stmt->execute()) {
	    $result = $stmt->fetchAll();
	    return $result[0];
	} else {
	    return FALSE;
	}

    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function sch_format_text ($text, $allow_breaks = FALSE) {

    $text = str_replace ("<", "&lt;", $text);

    $text = str_replace (">", "&gt;", $text);

    if ($allow_breaks) {
	$text = str_replace ("\n", "<br>", $text);	
    }

    return ($text);

}

function sch_change_presenter_acceptance ($pid, $status) {

    try {
	
	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	$stmt = $dbh->prepare("UPDATE `presenters` SET `presentation_accepted` = :status WHERE `id` = :pid");
	
	$stmt->bindParam(':status', $st);
	$stmt->bindParam(':pid', $pi);

	$pi = $pid;
	$st = $status;

	if ($stmt->execute()) {
	    return TRUE;
	} else {
	    return FALSE;
	}

    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function sch_change_moderator_acceptance ($mid, $status) {

    try {
	
	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	$stmt = $dbh->prepare("UPDATE `moderators` SET `moderator_accepted` = :status WHERE `id` = :mid");
	
	$stmt->bindParam(':status', $st);
	$stmt->bindParam(':mid', $mi);

	$mi = $mid;
	$st = $status;

	if ($stmt->execute()) {
	    return TRUE;
	} else {
	    return FALSE;
	}

    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function sch_all_approved_presenter_availabilities () {
    
    try {
	
	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	$stmt = $dbh->prepare("SELECT * FROM `presenters`, `presenter_availability` WHERE `presenters`.`id` = `presenter_availability`.`presenter` AND `presenters`.`presentation_accepted` = 1;");

	if ($stmt->execute()) {
	    $result = $stmt->fetchAll();
	    return $result;
	} else {
	    return FALSE;
	}

    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function sch_get_presenter_availabilities ($presenter) {
    
    try {
	
	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	$stmt = $dbh->prepare("SELECT * FROM `presenter_availability` WHERE `presenter` = :presenter");
	$stmt->bindParam(':presenter', $pid);

	$pid = $presenter;

	if ($stmt->execute()) {
	    $result = $stmt->fetchAll();
	    return $result;
	} else {
	    return FALSE;
	}

    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function sch_presenter_available ($availabilities, $presenter, $slot) {

    foreach ($availabilities as $avail) {
	if ($avail['presenter'] == $presenter & $avail['slot'] == $slot) {
	    return TRUE;
	}
    }

    return FALSE;
    
}

function sch_slot_taken ($availabilities, $presenters, $slot) {
    
    $taken = 0;

    foreach ($presenters as $present) {
	foreach ($availabilities as $avail) {
	    if ($present['id'] == $avail['presenter']) {
		if ($avail['slot'] == $slot) {
		    if ($present['assigned_slot'] == $slot) {
			$taken++;
		    }
		}
	    }
	    
	}
    }

    return $taken;
    
}

function sch_save_schedule ($schedule) {

    if (preg_match("/^([0-9]+[0-9,-]+[0-9]+|[0-9]+)$/", $schedule) | $schedule == "") {

	$selections = explode(",", $schedule);

	$presenters = sch_get_presenters ("accepted");

	$successes = 0;

	foreach ($presenters as $presenter) {

	    $assigned_slot = NULL;
	    
	    foreach ($selections as $sel) {
		if ($presenter['id'] == explode("-", $sel)[0]) {
		    $assigned_slot = explode("-", $sel)[1];
		}
	    }

	    try {
		
		$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
		$stmt = $dbh->prepare("UPDATE `presenters` SET `assigned_slot` = :as WHERE `id` = :pid;");

		$stmt->bindParam(':as', $as);
		$stmt->bindParam(':pid', $pi);

		$as = $assigned_slot;
		$pi = $presenter['id'];

		if ($stmt->execute()) {
		    $successes++;
		}

	    }

	    catch (PDOException $e) {

		echo $e->getMessage();

	    }
	    
	}

	if ($successes == count($presenters)) {
	    return TRUE;
	} else {
	    return FALSE;
	}
	
    } else {
	return FALSE;
    }

}

function sch_save_link ($pid, $link) {

    try {
	
	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	$stmt = $dbh->prepare("UPDATE `presenters` SET `hosting_details` = :hd WHERE `id` = :pid;");

	$stmt->bindParam(':hd', $hd);
	$stmt->bindParam(':pid', $pi);

	$hd = $link;
	$pi = $pid;

	if ($stmt->execute()) {
	    return TRUE;
	}

    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }

    return FALSE;
    
}

function sch_save_matched_mods ($matches) {

    if (preg_match("/^([0-9]+[0-9,-]+[0-9]+|[0-9]+)$/", $matches) | $matches == "") {

	$selections = explode(",", $matches);

	$presenters = sch_get_presenters ("confirmed");

	$successes = 0;

	foreach ($presenters as $presenter) {

	    $mod = NULL;
	    
	    foreach ($selections as $sel) {
		if ($presenter['id'] == explode("-", $sel)[0]) {
		    $mod = explode("-", $sel)[1];
		}
	    }

	    try {
		
		$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
		$stmt = $dbh->prepare("UPDATE `presenters` SET `matched_mod` = :mm WHERE `id` = :pid;");

		$stmt->bindParam(':mm', $mm);
		$stmt->bindParam(':pid', $pi);

		$mm = $mod;
		$pi = $presenter['id'];

		if ($stmt->execute()) {
		    $successes++;
		}

	    }

	    catch (PDOException $e) {

		echo $e->getMessage();

	    }
	    
	}

	if ($successes == count($presenters)) {
	    return TRUE;
	} else {
	    return FALSE;
	}
	
    } else {
	return FALSE;
    }

}

function sch_send_presenter_confirmation_email ($pid) {

    $all_presenters = sch_get_presenters("accepted");

    $presenter = sch_get_presenter ($pid);

    // Clear old links

    try {
	
	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	$stmt = $dbh->prepare("UPDATE `presenter_confirmation` SET `active` = 0 WHERE `presenter` = :pid");

	$stmt->bindParam(':pid', $pre);

	$pre = $pid;

	if ($stmt->execute()) {
	    $old_links_cleared = TRUE;
	} else {
	    $old_links_cleared = FALSE;
	    echo '<div class="alert alert-danger" role="alert">Database error, try again</div>';
	}

    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }

    if ($old_links_cleared) {

	// Generate a unique link
	$link_chosen = FALSE;
	while ($link_chosen == FALSE) {

	    try {
		
		$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
		$stmt = $dbh->prepare("SELECT * FROM `presenter_confirmation` WHERE `link` = :link");

		$stmt->bindParam(':link', $li);

		$li = substr(md5(uniqid(rand(), true)) , 0, 12);

		if ($stmt->execute()) {
		    $result = $stmt->fetchAll();
		    if (count ($result) == 0) {
			$link_chosen = TRUE;
			$link = $li;
		    }
		} else {
		    echo '<div class="alert alert-danger" role="alert">Database error, try again</div>';
		}

	    }

	    catch (PDOException $e) {

		echo $e->getMessage();

	    }
	    
	}
	
    }
    
    // Insert the link and the confirmation offer into the database
    // so that we know when the emails were sent and what slot was offered

    if ($link_chosen) {

	try {
	    
	    $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	    $stmt = $dbh->prepare("INSERT INTO `presenter_confirmation` (`presenter`, `slot`, `link`) VALUES (:pid, :slot, :link)");

	    $stmt->bindParam(':pid', $pi);
	    $stmt->bindParam(':slot', $sl);
	    $stmt->bindParam(':link', $li);

	    $pi = $pid;
	    $sl = $presenter['assigned_slot'];
	    $li = $link;

	    if ($stmt->execute()) {
		$confirmation_recorded = TRUE;
	    } else {
		$confirmation_recorded = FALSE;
		echo '<div class="alert alert-danger" role="alert">Database error, try again</div>';
	    }

	}

	catch (PDOException $e) {

	    echo $e->getMessage();

	}
	
    }

    // If there is a confirmation offer in the database, then send the email
    if ($confirmation_recorded) {
	$remove_confirmation = FALSE;

	$confirmation_deadline = date("Y-m-d (D) H:i", min(strtotime(MOD_SIGNUP_START), strtotime(PARTIC_SIGNUP_START))) . " UTC";

	$utc_time = date("Y-m-d (D) H:i", strtotime(CONF_START) + ($presenter['assigned_slot'] - 1) * 60 * 60) . " UTC";
	
	$subject = "Your presentation has been accepted; please confirm your presentation time";

	$body = "Dear " . $presenter['name'] . ",\n\n";
	$body .= "Thank you very much for submitting '" . $presenter['title'] . "' to " . CONF_NAME . ".\n\n";
	$body .= "We are pleased to accept your presentation and offer you the following presentation time:\n\n";
	$body .= $utc_time . "\n\n";
	$body .= "To confirm that you are available for this time, please use the following link:\n\n";
	$body .= SITE_URL . "presenters-signup/" . $link . "\n\n";
	$body .= "We request that you confirm your presentation time as soon as possible so that we can recruit moderators and participants for your presentation.\n\n";
	$body .= "Best regards,\n\nThe organization team\n" . CONF_NAME;
	

	$mail = new PHPMailer(true);

	try {
	    // Server settings
	    // $mail->SMTPDebug = 2;
	    $mail->isSMTP();
	    $mail->Host = SMTP_HOST;
	    $mail->SMTPAuth = true;
	    $mail->Username = SENDER_EMAIL;
	    $mail->Password = SENDER_PASS;
	    $mail->SMTPSecure = 'ssl';
	    $mail->Port = SMTP_PORT;

	    // Email headers
	    $mail->setFrom(SENDER_EMAIL, CONF_NAME);
	    $mail->addAddress($presenter['email']);
	    $mail->addReplyTo(SENDER_EMAIL, CONF_NAME);

	    // Email content
	    $mail->isHTML(false);
	    $mail->Subject = $subject;
	    $mail->Body = $body;

	    // Send email
	    $mail->send();
	    echo '<div class="alert alert-success" role="alert">Message sent</div>';
	    
	} catch (Exception $e) {
	    echo '<div class="alert alert-danger" role="alert">Database error, try again</div>';
	    $remove_confirmation = TRUE;
	}
	
    }

    if ($remove_confirmation) {

	try {
	    
	    $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	    $stmt = $dbh->prepare("DELETE FROM `presenter_confirmation` WHERE `link` = :link LIMIT 1");

	    $stmt->bindParam(':link', $li);

	    $li = $link;

	    if ($stmt->execute()) {
		echo '<div class="alert alert-danger" role="alert">Refresh this page to try sending again</div>';
	    }

	}

	catch (PDOException $e) {

	    echo $e->getMessage();

	}
	
    }

}

function sch_send_mod_confirmation_email ($mid) {

    $mod = sch_get_moderator ($mid);
    $presenters = sch_get_presenters ("confirmed-with-matched-mods");

    // Clear old links

    try {
	
	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	$stmt = $dbh->prepare("UPDATE `moderator_confirmation` SET `active` = 0 WHERE `moderator` = :mid");

	$stmt->bindParam(':mid', $mid);

	$mi = $mid;
	
	if ($stmt->execute()) {
	    $old_links_cleared = TRUE;
	} else {
	    $old_links_cleared = FALSE;
	    echo '<div class="alert alert-danger" role="alert">Database error, try again</div>';
	}

    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }

    if ($old_links_cleared) {

	// Generate a unique link
	$link_chosen = FALSE;
	while ($link_chosen == FALSE) {

	    try {
		
		$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
		$stmt = $dbh->prepare("SELECT * FROM `moderator_confirmation` WHERE `link` = :link");

		$stmt->bindParam(':link', $li);

		$li = substr(md5(uniqid(rand(), true)) , 0, 12);

		if ($stmt->execute()) {
		    $result = $stmt->fetchAll();
		    if (count ($result) == 0) {
			$link_chosen = TRUE;
			$link = $li;
		    }
		} else {
		    echo '<div class="alert alert-danger" role="alert">Database error, try again</div>';
		}

	    }

	    catch (PDOException $e) {

		echo $e->getMessage();

	    }
	    
	}
	
    }
    
    // Insert the link and the confirmation offer into the database
    // so that we know when the emails were sent and what slot was offered

    if ($link_chosen) {

	try {
	    
	    $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	    $stmt = $dbh->prepare("INSERT INTO `moderator_confirmation` (`moderator`, `link`) VALUES (:mid, :link)");

	    $stmt->bindParam(':mid', $mi);
	    $stmt->bindParam(':link', $li);

	    $mi = $mid;
	    $li = $link;

	    if ($stmt->execute()) {
		$confirmation_recorded = TRUE;
	    } else {
		$confirmation_recorded = FALSE;
		echo '<div class="alert alert-danger" role="alert">Database error, try again</div>';
	    }

	}

	catch (PDOException $e) {

	    echo $e->getMessage();

	}
	
    }

    // If there is a confirmation offer in the database, then send the email
    if ($confirmation_recorded) {

	$confirmation_deadline = date("Y-m-d (D) H:i", strtotime(MOD_SIGNUP_END) + 86400) . " UTC";
	
	$subject = "Thank you for volunteering to moderate at " . CONF_NAME . "; please confirm your availability";

	$body = "Dear " . $mod['name'] . ",\n\n";
	$body .= "Thank you very much for volunteering to act as a moderator at " . CONF_NAME . ".\n\n";
	$body .= "We have matched you with the following presentation(s) and request that you confirm that you are available to moderate these talks:\n\n";

	foreach ($presenters as $pre) {
	    if ($pre['moderators_id'] == $mod['id']) {
		$presentation_time_utc = date("Y-m-d (D) H:i", strtotime(CONF_START) + (($pre['confirmed_slot'] - 1) * 60 * 60)) . " UTC";

		if ($pre['pronouns'] != "") {
		    $presentation_pronouns = " (" . $pre['pronouns'] . ")";
		} else {
		    $presentation_pronouns = "";
		}
		
		$body .= "* '" . $pre['title'] ."' by " . $pre['name'] . $presentation_pronouns . " at " . $presentation_time_utc . "\n";
	    }
	}

	$body .= "\n";
	
	$body .= "Please confirm your availability using the following link as soon as possible, as we wish to have all moderator-presenter pairs confirmed by " . $confirmation_deadline . ".\n\n";
	$body .= SITE_URL . "moderators-signup/" . $link . "\n\n";
	$body .= "When you have confirmed your availability, we will email both you and the presenter to make introductions. We recommend that you briefly speak with each other to discuss the pronunciation of names, pronouns, how the presenter wishes to be introduced and whether any other accommodations need to be made.\n\n";
	$body .= "Best regards,\n\nThe organization team\n" . CONF_NAME;

	$mail = new PHPMailer(true);
	
	try {
	    // Server settings
	    // $mail->SMTPDebug = 2;
	    $mail->isSMTP();
	    $mail->Host = SMTP_HOST;
	    $mail->SMTPAuth = true;
	    $mail->Username = SENDER_EMAIL;
	    $mail->Password = SENDER_PASS;
	    $mail->SMTPSecure = 'ssl';
	    $mail->Port = SMTP_PORT;

	    // Email headers
	    $mail->setFrom(SENDER_EMAIL, CONF_NAME);
	    $mail->addAddress($mod['email']);
	    $mail->addReplyTo(SENDER_EMAIL, CONF_NAME);

	    // Email content
	    $mail->isHTML(false);
	    $mail->Subject = $subject;
	    $mail->Body = $body;

	    // Send email
	    $mail->send();
	    echo '<div class="alert alert-success" role="alert">Message sent</div>';
	    
	} catch (Exception $e) {
	    echo '<div class="alert alert-danger" role="alert">Database error, try again</div>';
	}
	
    }

}

function sch_get_presenter_confirmation_by_link ($link) {

    try {
	
	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	$stmt = $dbh->prepare("SELECT * FROM `presenter_confirmation` WHERE `link` = :link and `active` = 1");

	$stmt->bindParam(':link', $li);

	$li = $link;
	
	if ($stmt->execute()) {
	    $result = $stmt->fetchAll();
	    if (count($result) > 0) {
		return $result[0];
	    } else {
		return FALSE;
	    }
	} else {
	    return FALSE;
	}

    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function sch_get_moderator_confirmation_by_link ($link) {

    try {
	
	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	$stmt = $dbh->prepare("SELECT * FROM `moderator_confirmation` WHERE `link` = :link and `active` = 1");

	$stmt->bindParam(':link', $li);

	$li = $link;
	
	if ($stmt->execute()) {
	    $result = $stmt->fetchAll();
	    if (count($result) > 0) {
		return $result[0];
	    } else {
		return FALSE;
	    }
	} else {
	    return FALSE;
	}

    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function sch_save_moderator_confirmation_response ($confirm, $availability) {

    $mod = sch_get_moderator($confirm['moderator']);

    $presenters = sch_get_presenters ("confirmed-with-matched-mods");

    // Close the link
    $link_closed = FALSE;
    try {
	
	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	$stmt = $dbh->prepare("UPDATE `moderator_confirmation` SET `active` = '0' WHERE `link` = :link;");

	$stmt->bindParam(':link', $li);

	$li = $confirm['link'];

	if ($stmt->execute()) {
	    $link_closed = TRUE;
	}

    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }

    if ($link_closed & (preg_match("/^([0-9]+[0-9,-]+[0-9]+|[0-9]+)$/", $availability) | $availability == "")) {
	// Well-formed availability

	$checked = explode(",", $availability);

	foreach ($presenters as $pre) {
	    if ($pre['moderators_id'] == $mod['id']) {

		if (in_array($pre['id'], $checked)) {
		    // Mark as confirmed

		    $confirmation_success = FALSE;
		    
		    try {
			
			$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
			$stmt = $dbh->prepare("UPDATE `presenters` SET `confirmed_mod` = :mid WHERE `id` = :pid;");

			$stmt->bindParam(':pid', $pid);
			$stmt->bindParam(':mid', $mid);

			$pid = $pre['id'];
			$mid = $mod['id'];

			if ($stmt->execute()) {
			    $confirmation_success = TRUE;
			} else {
			    echo '<div class="alert alert-danger mb-3" role="alert">Database error saving moderation confirmation; please contact the organizers</div>';
			}

		    }

		    catch (PDOException $e) {

			echo $e->getMessage();

		    }

		    // Only continue in the case that the confirmation was saved
		    if ($confirmation_success) {

			// Check that we're not writing over already existing hosting details
			// and that the moderator has provided a link
			if ($pre['hosting_details'] == "" & $_POST['mod-confirm-link-' . $pre['id']] != "") {

			    try {
				
				$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
				$stmt = $dbh->prepare("UPDATE `presenters` SET `hosting_details` = :hd WHERE `id` = :pid;");

				$stmt->bindParam(':pid', $pid);
				$stmt->bindParam(':hd', $hd);

				$pid = $pre['id'];
				$hd = $_POST['mod-confirm-link-' . $pre['id']];

				if ($stmt->execute()) {
				    echo '<div class="alert alert-success mb-3" role="alert">Schedule successfully updated; you have been confirmed as the moderator for ' . $pre['name'] . '\'s presentation</div>';
				} else {
				    echo '<div class="alert alert-danger mb-3" role="alert">Database error saving moderation confirmation; please contact the organizers</div>';
				}

			    }

			    catch (PDOException $e) {

				echo $e->getMessage();

			    }
			    
			} else { // No hosting details to copy, success!
			    echo '<div class="alert alert-success mb-3" role="alert">Schedule successfully updated; you have been confirmed as the moderator for ' . $pre['name'] . '\'s presentation</div>';
			}
			
		    }
		    
		} else {
		    echo '<div class="alert alert-success mb-3" role="alert">Schedule successfully updated; we will find another moderator for ' . $pre['name'] . '\'s presentation</div>';
		}
		
	    }
	}
	
    }
    
}

function sch_save_presenter_confirmation_response ($link, $presenter_id, $slot, $confirm_time, $availability, $name, $pronouns, $handle, $title, $abstract, $host, $hosting_details, $recording, $transcript) {

    // Update the confirmation table
    try {
	
	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	$stmt = $dbh->prepare("UPDATE `presenter_confirmation` SET `active` = 0 WHERE `link` = :link;");

	$stmt->bindParam(':link', $li);

	$li = $link;

	if ($stmt->execute()) {
	    $confirmation_table_update_success = TRUE;
	} else {
	    $confirmation_table_update_success = FALSE;
	}
	
    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
    // Update the presentation details and set the `confirmed_slot` column
    
    if ($confirmation_table_update_success) {

	switch ($confirm_time) {
		
	    case "yes": // Presenter confirms presentation at the assigned time

		// Update details
		try {
		    
		    $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
		    $stmt = $dbh->prepare("UPDATE `presenters` SET `name` = :name, `pronouns` = :pronouns, `handle` = :handle, `title` = :title, `abstract` = :abstract, `able_to_host` = :host, `hosting_details` = :hosting_details, `consent_to_record` = :recording, `provide_transcript` = :transcript, `confirmed_slot` = :slot WHERE `id` = :pid;");

		    $stmt->bindParam(':name', $na);
		    $stmt->bindParam(':pronouns', $pr);
		    $stmt->bindParam(':handle', $ha);
		    $stmt->bindParam(':title', $ti);
		    $stmt->bindParam(':abstract', $ab);
		    $stmt->bindParam(':host', $ho);
		    $stmt->bindParam(':hosting_details', $hd);
		    $stmt->bindParam(':recording', $re);
		    $stmt->bindParam(':transcript', $tr);
		    $stmt->bindParam(':slot', $sl);
		    $stmt->bindParam(':pid', $pid);

		    $na = $name;
		    $pr = $pronouns;
		    $ha = $handle;
		    $ti = $title;
		    $ab = $abstract;
		    $ho = $host;
		    $hd = $hosting_details;
		    $re = $recording;
		    $tr = $transcript;
		    $sl = $slot;
		    $pid = $presenter_id;

		    if ($stmt->execute()) {
			echo "<p>Thank you! An organizer will contact you when you have been matched with a moderator for your presentation.</p>";
		    }

		}

		catch (PDOException $e) {

		    echo $e->getMessage();

		}
		
		break;
		
	    case "no": // Presenter would like to present at a different time

		// If the input is well-formed
		if (preg_match("/^([0-9]+[0-9,]+[0-9]+|[0-9]+)$/", $availability)) {
		    
		    // Clear old availabilities
		    try {
			
			$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
			$stmt = $dbh->prepare("DELETE FROM `presenter_availability` WHERE `presenter` = :pid;");

			$stmt->bindParam(':pid', $pid);

			$pid = $presenter_id;

			if ($stmt->execute()) {
			    $cleared_old_avail = TRUE;
			} else {
			    $cleared_old_avail = FALSE;
			    echo "<p>Error saving response.</p>";
			}

		    }

		    catch (PDOException $e) {

			echo $e->getMessage();

		    }
		    
		} else {
		    echo "<p>Availability input is not well-formed</p>";
		}

		// Un-schedule the presenter

		if ($cleared_old_avail) {
		    
		    try {
			
			$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
			$stmt = $dbh->prepare("UPDATE `presenters` SET `assigned_slot` = NULL WHERE `id` = :pid;");

			$stmt->bindParam(':pid', $pid);

			$pid = $presenter_id;

			if ($stmt->execute()) {
			    $unscheduled_presenter = TRUE;
			} else {
			    $unscheduled_presenter = FALSE;
			    echo "<p>Error saving response.</p>";
			}

		    }

		    catch (PDOException $e) {

			echo $e->getMessage();

		    }
		}

		// Enter new availabilities

		if ($unscheduled_presenter) {

		    $slots = explode(",", $availability);

		    $avail_query = "INSERT INTO `presenter_availability`(presenter, slot) VALUES ";

		    $slots_queries = [];
		    
		    foreach ($slots as $slot) {
			array_push($slots_queries, "(" . $presenter_id . "," . $slot . ")");
		    }

		    $avail_query .= implode(", ", $slots_queries);

		    try {
			
			$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
			$stmt = $dbh->prepare($avail_query);

			if ($stmt->execute()) {
			    echo "<p>Your availability has been successfully edited. We will try to reschedule your presentation and confirm with you shortly!</p>";
			} else {
			    echo "<p>Database connection error. Please contact the organizers.</p>";
			}

		    }

		    catch (PDOException $e) {

			echo $e->getMessage();

		    }
		    
		}
		
		break;
		
	    case "never": // Presenter would like to withdraw from the conference

		// Update presenter details
		try {
		    
		    $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
		    $stmt = $dbh->prepare("UPDATE `presenters` SET `confirmed_slot` = NULL, `presentation_accepted` = 3 WHERE `id` = :pid;");

		    $stmt->bindParam(':pid', $pid);

		    $pid = $presenter_id;

		    if ($stmt->execute()) {
			echo "<p>Your presentation has been withdrawn.</p>";
		    } else {
			echo "<p>Error saving response.</p>";
		    }

		}

		catch (PDOException $e) {

		    echo $e->getMessage();

		}
		
		break;
		
	}
	
    }

}

function sch_get_all_links_for_moderator ($mid) {

    try {
	
	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	$stmt = $dbh->prepare("SELECT * FROM `moderator_confirmation` WHERE `moderator` = :mid");

	$stmt->bindParam(':mid', $mo);

	$mo = $mid;
	
	if ($stmt->execute()) {
	    $result = $stmt->fetchAll();
	    if (count($result) > 0) {
		return $result;
	    } else {
		return FALSE;
	    }
	} else {
	    return FALSE;
	}

    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function sch_get_all_links_for_presenter ($pid) {

    try {
	
	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	$stmt = $dbh->prepare("SELECT * FROM `presenter_confirmation` WHERE `presenter` = :pid");

	$stmt->bindParam(':pid', $pr);

	$pr = $pid;
	
	if ($stmt->execute()) {
	    $result = $stmt->fetchAll();
	    if (count($result) > 0) {
		return $result;
	    } else {
		return FALSE;
	    }
	} else {
	    return FALSE;
	}

    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function sch_clear_presenter_confirmation ($pid) {

    // Clear old links

    try {
	
	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	$stmt = $dbh->prepare("UPDATE `presenter_confirmation` SET `active` = 0 WHERE `presenter` = :pid");

	$stmt->bindParam(':pid', $pre);

	$pre = $pid;

	if ($stmt->execute()) {
	    $old_links_cleared = TRUE;
	} else {
	    $old_links_cleared = FALSE;
	    echo '<div class="alert alert-danger" role="alert">Database error, try again</div>';
	}

    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }

    if ($old_links_cleared) {

	// Clear confirmation

	try {
	    
	    $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	    $stmt = $dbh->prepare("UPDATE `presenters` SET `confirmed_slot` = NULL WHERE `id` = :pid;");

	    $stmt->bindParam(':pid', $pi);

	    $pi = $pid;

	    if ($stmt->execute()) {
		echo '<div class="alert alert-success" role="alert">Confirmation and links cleared</div>';
	    } else {
		echo '<div class="alert alert-danger" role="alert">Database error, try again</div>';
	    }

	}

	catch (PDOException $e) {

	    echo $e->getMessage();

	}
	
    }
    
}

function sch_clear_mod_confirmation ($mid) {
    
    // Clear old links

    try {
	
	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	$stmt = $dbh->prepare("UPDATE `moderator_confirmation` SET `active` = 0 WHERE `moderator` = :mid");

	$stmt->bindParam(':mid', $mi);

	$mi = $mid;
	
	if ($stmt->execute()) {
	    $old_links_cleared = TRUE;
	} else {
	    $old_links_cleared = FALSE;
	    echo '<div class="alert alert-danger" role="alert">Database error, try again</div>';
	}

    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }

    if ($old_links_cleared) {

	// Clear confirmation

	try {
	    
	    $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	    $stmt = $dbh->prepare("UPDATE `presenters` SET `confirmed_mod` = NULL WHERE `confirmed_mod` = :mid;");

	    $stmt->bindParam(':mid', $mi);

	    $mi = $mid;
	    
	    if ($stmt->execute()) {
		echo '<div class="alert alert-success" role="alert">Confirmation and links cleared</div>';
	    } else {
		echo '<div class="alert alert-danger" role="alert">Database error, try again</div>';
	    }

	}

	catch (PDOException $e) {

	    echo $e->getMessage();

	}
	
    }
    
}

function sch_save_participant ($partic_email, $handle, $presentations) {

    try {
	
	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	$stmt = $dbh->prepare("SELECT * FROM `participants` WHERE `email` = :email");

	$stmt->bindParam(':email', $em);

	$em = $partic_email;

	if ($stmt->execute()) {
	    $result = $stmt->fetchAll();
	    if (count($result) == 0) {
		$email_not_taken = TRUE;
	    } else {
		$email_not_taken = FALSE;
		echo "<p>This email has already been used to sign up to participate at " . CONF_NAME . ". Please use the 'View or change saved attendance plans' button above.</p>";
	    }
	} else {
	    $email_not_taken = FALSE;
	    echo "<p>Database error. Contact the organizers.</p>";
	}

    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }

    if ($email_not_taken) {
	
	try {
	    
	    $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	    $stmt = $dbh->prepare("INSERT INTO `participants` (`email`, `handle`) VALUES (:email, :handle);");

	    $stmt->bindParam(':handle', $ha);
	    $stmt->bindParam(':email', $em);

	    $ha = $handle;
	    $em = $partic_email;

	    if ($stmt->execute()) {
		$details_saved = TRUE;
		
		$stmt2 = $dbh->prepare("SELECT LAST_INSERT_ID() AS newid;");
		$stmt2->execute();

		$result = $stmt2->fetchAll();

		$newid = $result[0]['newid'];
		
	    } else {
		$details_saved = FALSE;
		echo "<p>Error saving participant details. Please contact the organizers.</p>";
	    }

	}

	catch (PDOException $e) {

	    echo $e->getMessage();

	}
	
    }

    if ($details_saved) {

	$present = explode(",", $presentations);

	$present_query = "INSERT INTO `participant_signups` (participant, presenter) VALUES ";

	$present_queries = [];
	
	foreach ($present as $pre) {
	    array_push($present_queries, "(" . $newid . "," . $pre . ")");
	}

	$present_query .= implode(", ", $present_queries);

	try {
	    
	    $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	    $stmt = $dbh->prepare($present_query);

	    if ($stmt->execute()) {
		$presentations_saved = TRUE;
	    } else {
		$presentations_saved = FALSE;
		echo "<p>Error saving attendance plans. Please contact the organizers.</p>";
	    }

	}

	catch (PDOException $e) {

	    echo $e->getMessage();

	}
	
    }

    if ($presentations_saved) {
	// Generate a unique link
	$link_chosen = FALSE;
	while ($link_chosen == FALSE) {

	    try {
		
		$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
		$stmt = $dbh->prepare("SELECT * FROM `participants` WHERE `link` = :link");

		$stmt->bindParam(':link', $li);

		$li = substr(md5(uniqid(rand(), true)) , 0, 12);

		if ($stmt->execute()) {
		    $result = $stmt->fetchAll();
		    if (count ($result) == 0) {
			$link_chosen = TRUE;
			$link = $li;
		    }
		} else {
		    echo '<div class="alert alert-danger" role="alert">Database error, try again 1</div>';
		}

	    }

	    catch (PDOException $e) {

		echo $e->getMessage();

	    }
	    
	}
	
    }

    if ($link_chosen) {
	
	try {
	    
	    $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	    $stmt = $dbh->prepare("UPDATE `participants` SET `link` = :link, `when_link_sent` = NOW() WHERE `id` = :nid");
	    
	    $stmt->bindParam(':link', $li);
	    $stmt->bindParam(':nid', $ni);

	    $li = $link;
	    $ni = $newid;
	    
	    if ($stmt->execute()) {
		$link_set = TRUE;
	    } else {
		$link_set = FALSE;
		echo '<div class="alert alert-danger" role="alert">Database error, try again 2</div>';
	    }

	}

	catch (PDOException $e) {

	    echo $e->getMessage();

	}
	
    }

    // Send email
    if ($link_set) {

	$body = "Thank you for signing up to attend " . CONF_NAME . ". Please verify your email address by clicking the link below:\n\n";

	$body .= SITE_URL . "programme/" . $link . "\n\n";

	$body .= "Best regards,\n\nThe organization team\n" . CONF_NAME;

	if (sch_email ($partic_email, "Please verify your email", $body)) {
	    echo '<div class="alert alert-success" role="alert">Saved! Verify your email to receive the link to the web conference rooms on the day of each presentation.</div>';
	} else {
	    echo '<div class="alert alert-danger" role="alert">Unable to send email</div>';
	}
	
    }
    
}

function sch_participant_link ($link) {
    
    try {
	
	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	$stmt = $dbh->prepare("SELECT * FROM `participants` WHERE `link` = :link");

	$stmt->bindParam(':link', $li);

	$li = $link;
	
	if ($stmt->execute()) {
	    $result = $stmt->fetchAll();
	} else {
	    return FALSE;
	}

    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }

    $row = $result[0];

    // Verify email, if necessary
    if (count($result) == 1 & $row['email_verified'] == 0) {

	try {
	    
	    $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	    $stmt = $dbh->prepare("UPDATE `participants` SET `email_verified` = 1 WHERE `id` = :pid");
	    
	    $stmt->bindParam(':pid', $pi);

	    $pi = $row['id'];

	    $stmt->execute();

	}

	catch (PDOException $e) {

	    echo $e->getMessage();

	}

    }

    if (count($result) != 1) {
	return FALSE;
    }

    if ($row['email_verified'] == 1 & strtotime($row['when_link_sent']) + 86400 * 3 < time()) {
	return FALSE;
    }

    return $row;

}

function sch_get_participant_signups ($participant) {

    try {
	
	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	$stmt = $dbh->prepare("SELECT * FROM `participant_signups` WHERE `participant` = :part");

	$stmt->bindParam(':part', $part);

	$part = $participant;
	
	if ($stmt->execute()) {
	    $result = $stmt->fetchAll();
	    return $result;
	} else {
	    return FALSE;
	}

    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function sch_update_participant ($participant, $presentations) {

    // Clear old availabilities

    try {
	
	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	$stmt = $dbh->prepare("DELETE FROM `participant_signups` WHERE `participant` = :pid;");

	$stmt->bindParam(':pid', $pid);

	$pid = $participant;

	if ($stmt->execute()) {
	    $cleared_old_partic = TRUE;
	} else {
	    $cleared_old_partic = FALSE;
	}

    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }

    if ($cleared_old_partic) {
	
	$present = explode(",", $presentations);

	$present_query = "INSERT INTO `participant_signups` (participant, presenter) VALUES ";

	$present_queries = [];
	
	foreach ($present as $pre) {
	    array_push($present_queries, "(" . $participant . "," . $pre . ")");
	}

	$present_query .= implode(", ", $present_queries);

	try {
	    
	    $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	    $stmt = $dbh->prepare($present_query);

	    if ($stmt->execute()) {
		$presentations_saved = TRUE;
	    } else {
		$presentations_saved = FALSE;
	    }

	}

	catch (PDOException $e) {

	    echo $e->getMessage();

	}
	
    }

    return $presentations_saved;
    
}

function sch_send_participant_signup_email ($email) {

    try {
	
	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	$stmt = $dbh->prepare("SELECT * FROM `participants` WHERE `email` = :em");

	$stmt->bindParam(':em', $em);

	$em = $email;
	
	if ($stmt->execute()) {
	    $result = $stmt->fetchAll();
	    $participant = $result[0];
	    $participant_found = TRUE;
	} else {
	    $participant_found = FALSE;
	    echo '<div class="alert alert-danger" role="alert">Database error</div>';
	    
	}

    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }

    if ($participant_found) {
	// Generate a unique link
	$link_chosen = FALSE;
	while ($link_chosen == FALSE) {

	    try {
		
		$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
		$stmt = $dbh->prepare("SELECT * FROM `participants` WHERE `link` = :link");

		$stmt->bindParam(':link', $li);

		$li = substr(md5(uniqid(rand(), true)) , 0, 12);

		if ($stmt->execute()) {
		    $result = $stmt->fetchAll();
		    if (count ($result) == 0) {
			$link_chosen = TRUE;
			$link = $li;
		    }
		} else {
		    echo '<div class="alert alert-danger" role="alert">Database error, try again 1</div>';
		}

	    }

	    catch (PDOException $e) {

		echo $e->getMessage();

	    }
	    
	}
	
    }

    if ($link_chosen) {

	try {
	    
	    $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	    $stmt = $dbh->prepare("UPDATE `participants` SET `link` = :link, `when_link_sent` = NOW() WHERE `id` = :pid");
	    
	    $stmt->bindParam(':link', $li);
	    $stmt->bindParam(':pid', $pi);

	    $li = $link;
	    $pi = $participant['id'];
	    
	    if ($stmt->execute()) {
		$link_set = TRUE;
	    } else {
		$link_set = FALSE;
		echo '<div class="alert alert-danger" role="alert">Database error, try again 2</div>';
	    }

	}

	catch (PDOException $e) {

	    echo $e->getMessage();

	}
	
    }

    if ($link_set) {
	
	$body = "Thank you for signing up to attend " . CONF_NAME . ". You may edit your attendance plans using the link below:\n\n";

	$body .= SITE_URL . "programme/" . $link . "\n\n";

	$body .= "This link will be valid for 72 hours only.\n\n";

	$body .= "Best regards,\n\nThe organization team\n" . CONF_NAME;

	if (sch_email ($participant['email'], "Link to attendance plan", $body)) {
	    echo '<div class="alert alert-success" role="alert">Sent! Check your email (it may be in your spam folder)</div>';
	} else {
	    echo '<div class="alert alert-danger" role="alert">Unable to send email</div>';
	}
	
    }
    
}

function sch_get_moderators ($show = "all") {

    switch ($show) {
	case "accepted":
	    $query = "SELECT * FROM `moderators` WHERE `moderator_accepted` = 1";
	    break;
	case "matched":
	    $query = "SELECT * FROM `moderators` WHERE `moderators`.`id` IN (SELECT `presenters`.`matched_mod` FROM `presenters`)";
	    break;
	case "all":
	default:
	    $query = "SELECT * FROM `moderators`";
	    break;
    }

    try {
	
	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	$stmt = $dbh->prepare($query);
	
	if ($stmt->execute()) {
	    $result = $stmt->fetchAll();
	    return $result;
	} else {
	    return FALSE;
	}

    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function sch_get_moderator ($mid) {
    
    try {
 	
 	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
 	$stmt = $dbh->prepare("SELECT * FROM `moderators` WHERE `id` = :mid;");
	
 	
 	$stmt->bindParam(':mid', $mi);
	
 	$mi = $mid;
 	
 	if ($stmt->execute()) {
 	    $result = $stmt->fetchAll();
 	    return $result[0];
 	} else {
 	    return FALSE;
 	}
	
    }
    
    catch (PDOException $e) {
	
 	echo $e->getMessage();
	
    }
    
}

function sch_get_moderator_availabilities () {

    try {
	
	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	$stmt = $dbh->prepare("SELECT * FROM `moderators`, `moderator_availability` WHERE `moderators`.`id` = `moderator_availability`.`moderator`");
	
	if ($stmt->execute()) {
	    $result = $stmt->fetchAll();
	    return $result;
	} else {
	    return FALSE;
	}

    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function sch_mod_is_avail ($availabilities, $moderator, $presenter, $presenter_email) {

    foreach ($availabilities as $avail) {

	if ($avail['moderator'] == $moderator) {

	    if ($avail['presentation'] == $presenter) {

		if ($avail['email'] != $presenter_email) {

		    return $avail;
		    
		}
		
	    }
	    
	}
	
    }

    return FALSE;
    
}

function sch_check_feedback_link ($link) {
    
    try {
	
	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	$stmt = $dbh->prepare("SELECT * FROM `feedback_link` WHERE `link` = :link;");
	
	$stmt->bindParam(':link', $li);

	$li = $link;
	
	if ($stmt->execute()) {
	    $result = $stmt->fetchAll();
	    if (count ($result) > 0) {
		return TRUE;
	    }
	}

    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }

    return FALSE;
    
}

function sch_save_feedback ($link, $contact, $comments) {

    try {
	
	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	$stmt = $dbh->prepare("INSERT INTO `feedback` (`link`, `contact`, `comments`) VALUES (:link, :contact, :comments)");

	$stmt->bindParam(':link', $li);
	$stmt->bindParam(':contact', $cn);
	$stmt->bindParam(':comments', $cm);

	$li = $link;
	$cn = $contact;
	$cm = $comments;

	if ($stmt->execute()) {
	    return TRUE;
	}

    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }

    return FALSE;
    
}

function sch_feedback_link ($presenter, $role) {

    try {
	
	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	$stmt = $dbh->prepare("SELECT * FROM `feedback_link` WHERE `presenter` = :presenter AND `role` = :role;");

	$stmt->bindParam(':presenter', $pr);
	$stmt->bindParam(':role', $ro);

	$pr = $presenter;
	$ro = $role;
	
	if ($stmt->execute()) {
	    $result = $stmt->fetchAll();
	    if (count ($result) > 0) {
		return $result[0]['link'];
	    }
	}

    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }

    $link_chosen = FALSE;
    while ($link_chosen == FALSE) {

	try {
	    
	    $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	    $stmt = $dbh->prepare("SELECT * FROM `feedback_link` WHERE `link` = :link");

	    $stmt->bindParam(':link', $li);

	    $li = substr(md5(uniqid(rand(), true)) , 0, 12);

	    if ($stmt->execute()) {
		$result = $stmt->fetchAll();
		if (count ($result) == 0) {
		    $link_chosen = TRUE;
		    $link = $li;
		}
	    }

	}

	catch (PDOException $e) {

	    echo $e->getMessage();

	}
	
    }

    if ($link_chosen) {
	
	try {
	    
	    $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	    $stmt = $dbh->prepare("INSERT INTO `feedback_link` (`presenter`, `role`, `link`) VALUES (:presenter, :role, :link)");

	    $stmt->bindParam(':presenter', $pr);
	    $stmt->bindParam(':role', $ro);
	    $stmt->bindParam(':link', $li);

	    $pr = $presenter;
	    $ro = $role;
	    $li = $link;

	    if ($stmt->execute()) {
		return $link;
	    }

	}

	catch (PDOException $e) {

	    echo $e->getMessage();

	}
	
    }

    return FALSE;
    
}

function sch_send_moderator_intro_email ($pid) {

    $pre = sch_get_presenter ($pid);
    $mod = sch_get_moderator ($pre['confirmed_mod']);
    $presentation_time_utc = date("Y-m-d (D) H:i", strtotime(CONF_START) + (($pre['confirmed_slot'] - 1) * 60 * 60)) . " UTC";
    $link = sch_feedback_link ($pre['id'], "participant");

    $subject = "Final " . CONF_NAME . " moderation details";
    
    $body = "Dear " . $mod['name'] . ",\n\n";
    $body .= "Thank you very much for volunteering to moderate at " . CONF_NAME . ". We have finalized your moderation details and matched you with a presenter.\n\n";
    $body .= "You will be moderating '" . $pre['title'] . "' by " . $pre['name'];
    if ($pre['pronouns'] != "") {
	$body .= " (" . $pre['pronouns'] . ")";
    }
    $body .= ". Please email your presenter at " . $pre['email'] . " to arrange a time to discuss introducing the presenter and the topic.\n\n";
    $body .= "The presentation time will be: " . $presentation_time_utc . ".\n\n";
    $body .= "You can download your moderation appointment as an .ics file at the following link:\n\n";
    $body .= SITE_URL . "programme/calendar.php?presenter=" . $pre['id'] . "\n\n";
    $body .= "The following is the link to the web conference room where you will be moderating:\n\n";
    $body .= $pre['hosting_details'] . "\n\n";
    $body .= "Please familiarize yourself with the expectations for moderators at the following address:\n\n";
    $body .= SITE_URL . "moderators/\n\n";
    $body .= "At the end of the presentation and discussion, please provide the participants with following link for feedback to be sent to the conference organizers:\n\n";
    $body .= SITE_URL . "feedback/" . $link . "\n\n";
    $body .= "If you have any questions, please contact us: " . SITE_URL . "contact/\n\n";
    $body .= "Best regards,\n\nThe organization team\n" . CONF_NAME;

    if (sch_email ($mod['email'], $subject, $body)) {
	if (sch_record_intro_email ($pre['id'], "moderator")) {
	    return TRUE;
	}
    }

    return FALSE;
    
}

function sch_send_presenter_intro_email ($pid) {

    $pre = sch_get_presenter ($pid);
    $mod = sch_get_moderator ($pre['confirmed_mod']);
    $presentation_time_utc = date("Y-m-d (D) H:i", strtotime(CONF_START) + (($pre['confirmed_slot'] - 1) * 60 * 60)) . " UTC";
    $link = sch_feedback_link ($pre['id'], "participant");

    $subject = "Final " . CONF_NAME . " presentation details";
    
    $body = "Dear " . $pre['name'] . ",\n\n";
    $body .= "Thank you very much for volunteering to present at " . CONF_NAME . ". We have finalized your presentation details and matched you with a moderator.\n\n";
    $body .= "Your moderator will be " . $mod['name'];
    if ($mod['pronouns'] != "") {
	$body .= " (" . $mod['pronouns'] . ")";
    }
    $body .= ". Please email your moderator at " . $mod['email'] . " to arrange a time to discuss how you would like to be introduced.\n\n";
    $body .= "The presentation time will be: " . $presentation_time_utc . ". Please keep your presentation to 15 minutes, and allow the remainder of the hour for discussion.\n\n";
    $body .= "You can download your presentation appointment as an .ics file at the following link:\n\n";
    $body .= SITE_URL . "programme/calendar.php?presenter=" . $pre['id'] . "\n\n";
    $body .= "The following is the link to the web conference room where you will be presenting:\n\n";
    $body .= $pre['hosting_details'] . "\n\n";
    $body .= "At the end of the presentation and discussion, your moderator will provide the participants with following link for feedback to be sent to the conference organizers:\n\n";
    $body .= SITE_URL . "feedback/" . $link . "\n\n";
    $body .= "If you have any questions, please contact us: " . SITE_URL . "contact/\n\n";
    $body .= "Best regards,\n\nThe organization team\n" . CONF_NAME;

    if (sch_email ($pre['email'], $subject, $body)) {
	if (sch_record_intro_email ($pre['id'], "presenter")) {
	    return TRUE;
	}
    }

    return FALSE;
    
}

function sch_record_intro_email ($pid, $role) {
    
    try {
	
	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	$stmt = $dbh->prepare("INSERT INTO `intro_emails_sent` (`presenter`, `role`) VALUES (:presenter, :role)");

	$stmt->bindParam(':presenter', $pr);
	$stmt->bindParam(':role', $ro);

	$pr = $pid;
	$ro = $role;

	if ($stmt->execute()) {
	    return TRUE;
	}

    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }

    return FALSE;
    
}

function sch_get_sent_intro_emails () {
    
    try {
	
	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	$stmt = $dbh->prepare("SELECT * FROM `intro_emails_sent`");

	if ($stmt->execute()) {
	    $result = $stmt->fetchAll();
	    return $result;
	}

    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }

    return FALSE;
    
}

function sch_get_feedback () {
    
    try {
	
	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	$stmt = $dbh->prepare("SELECT *, CONVERT_TZ(`feedback`.`when_sent`, @@session.time_zone, '+00:00') as `utc_when_sent` FROM `presenters`, `feedback_link`, `feedback` WHERE `presenters`.`id` = `feedback_link`.`presenter` AND `feedback_link`.`link` = `feedback`.`link`;");

	if ($stmt->execute()) {
	    $result = $stmt->fetchAll();
	    return $result;
	}

    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }

    return FALSE;
    
}

function sch_get_participants ($presenter = "all", $status = "all") {

    $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
    
    if ($presenter == "all") {

	if ($status == "all") {
	    
	    $stmt = $dbh->prepare("SELECT * FROM `participants`, `participant_signups` WHERE `participants`.`id` = `participant_signups`.`participant`");
	    
	} else {
	    
	    $stmt = $dbh->prepare("SELECT * FROM `participants`, `participant_signups` WHERE `participants`.`id` = `participant_signups`.`participant` AND `participant_signups`.`accepted` = 1");
	    
	}
	
	
    } else {

	if ($status == "all") {
	    
	    $stmt = $dbh->prepare("SELECT * FROM `participants`, `participant_signups` WHERE `participants`.`id` = `participant_signups`.`participant` AND `participant_signups`.`presenter` = :presenter");
	    
	} else {
	    
	    $stmt = $dbh->prepare("SELECT * FROM `participants`, `participant_signups` WHERE `participants`.`id` = `participant_signups`.`participant` AND `participant_signups`.`presenter` = :presenter AND `participant_signups`.`accepted` = 1");
	    
	}
	
	$stmt->bindParam(':presenter', $pr);

	$pr = $presenter;
	
    }
    
    try {
	
	if ($stmt->execute()) {
	    $result = $stmt->fetchAll();
	    return $result;
	}

    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }

    return FALSE;
    
}

function sch_update_participant_signup_status ($participant, $newstatus) {
    
    try {
	
	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	$stmt = $dbh->prepare("UPDATE `participant_signups` SET `accepted` = :acc WHERE `id` = :pid");
	
	$stmt->bindParam(':pid', $pi);
	$stmt->bindParam(':acc', $acc);

	$pi = $participant;
	$acc = $newstatus;
	
	if ($stmt->execute()) {
	    return TRUE;
	}
	
    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }

    return FALSE;
    
}

function sch_send_participant_invites ($presenter_id) {

    $pre = sch_get_presenter ($presenter_id);
    $mod = sch_get_moderator ($pre['confirmed_mod']);
    $presentation_time_utc = date("Y-m-d (D) H:i", strtotime(CONF_START) + (($pre['confirmed_slot'] - 1) * 60 * 60)) . " UTC";

    $participants = sch_get_participants ($presenter_id, "accepted");

    $recipient_emails = array_column($participants, "email");
    array_push($recipient_emails, $pre['email']);
    array_push($recipient_emails, $mod['email']);

    $subject = "Details for " . CONF_NAME . " presentation: " . $pre['title'];

    $body = "Thank you for your interest in attending '" . $pre['title'] . "' by " . $pre['name'] . " at " . CONF_NAME . ".\n\n";

    $body .= "This presentation will begin at: " . $presentation_time_utc . "\n\n";

    $body .= "You can join the presentation at the following link:\n\n";

    $body .= $pre['hosting_details'] . "\n\n";

    $body .= "Please make every effort to arrive on time, as the presentation will be only 15 minutes long.\n\n";

    $body .= "You can familiarize yourself with the rules for participants at the following address:\n\n";

    $body .= SITE_URL . "participants/\n\n";
    
    $body .= "If you have any questions, please contact us: " . SITE_URL . "contact/\n\n";
    
    $body .= "Best regards,\n\nThe organization team\n" . CONF_NAME;

    if (sch_email_many ($recipient_emails, $subject, $body)) {
	
	try {
	    
	    $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	    $stmt = $dbh->prepare("INSERT INTO `invite_emails_sent` (`presenter`) VALUES (:pre)");
	    
	    $stmt->bindParam(':pre', $pre);

	    $pre = $presenter_id;
	    
	    if ($stmt->execute()) {
		return TRUE;
	    }
	    
	}

	catch (PDOException $e) {

	    echo $e->getMessage();

	}
    }

    return FALSE;
    
}

function sch_get_sent_invite_emails () {
    
    try {
	
	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	$stmt = $dbh->prepare("SELECT * FROM `invite_emails_sent`;");

	if ($stmt->execute()) {
	    $result = $stmt->fetchAll();
	    return $result;
	}

    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }

    return FALSE;

}

function sch_add_archive_link ($presenter_id) {

    $link_chosen = FALSE;
    while ($link_chosen == FALSE) {

	try {
	    
	    $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	    $stmt = $dbh->prepare("SELECT * FROM `archive_link` WHERE `link` = :link");

	    $stmt->bindParam(':link', $li);

	    $li = substr(md5(uniqid(rand(), true)) , 0, 12);

	    if ($stmt->execute()) {
		$result = $stmt->fetchAll();
		if (count ($result) == 0) {
		    $link_chosen = TRUE;
		    $link = $li;
		}
	    }

	}

	catch (PDOException $e) {

	    echo $e->getMessage();

	}
	
    }

    if ($link_chosen) {
	
	try {
	    
	    $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	    $stmt = $dbh->prepare("UPDATE `archive_link` SET `active` = 0 WHERE `presenter` = :pre");

	    $stmt->bindParam(':pre', $pre);

	    $pre = $presenter_id;
	    
	    $stmt->execute();
	    
	}

	catch (PDOException $e) {

	    echo $e->getMessage();

	}
	
	try {
	    
	    $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	    $stmt = $dbh->prepare("INSERT INTO `archive_link` (`presenter`, `link`) VALUES (:pre, :link)");

	    $stmt->bindParam(':pre', $pre);
	    $stmt->bindParam(':link', $li);

	    $pre = $presenter_id;
	    $li = $link;
	    
	    if ($stmt->execute()) {
		return $link;
	    } else {
		return FALSE;
	    }

	}

	catch (PDOException $e) {

	    echo $e->getMessage();

	}	
	
    }
    
}

function sch_get_archive_link ($link) {
    
    try {
	
	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	$stmt = $dbh->prepare("SELECT * FROM `archive_link` WHERE `link` = :link AND `active` = 1 AND `when_link_sent` > NOW() - INTERVAL 72 HOUR");

	$stmt->bindParam(':link', $li);

	$li = $link;
	
	if ($stmt->execute()) {
	    $result = $stmt->fetchAll();
	    if (count($result) > 0) {
		return $result[0];
	    } else {
		return FALSE;
	    }
	} else {
	    return FALSE;
	}

    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function sch_get_archive_links_for_presentation ($presentation_id) {

    try {
	
	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	$stmt = $dbh->prepare("SELECT * FROM `archive_link` WHERE `presenter` = :pre;");

	$stmt->bindParam(':pre', $pid);

	$pid = $presentation_id;
	
	if ($stmt->execute()) {
	    $result = $stmt->fetchAll();
	    return $result;
	} else {
	    return FALSE;
	}

    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function sch_update_presenter_archive_hidden ($presenter_id, $hider = "admin", $new_status = 0) {
    // $hider can be "admin" or "presenter", default "admin"
    // $new_status can be 1 or 0, 1 means hidden, 0 means not hidden

    if ($hider == "presenter") {
	$query = "UPDATE `presenters` SET `hidden_from_archive_presenter` = :newval WHERE `id` = :pid";
    } else {
	$query = "UPDATE `presenters` SET `hidden_from_archive_admin` = :newval WHERE `id` = :pid";
    }

    try {
	
	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	$stmt = $dbh->prepare($query);

	$stmt->bindParam(':pid', $pid);
	$stmt->bindParam(':newval', $nv);

	$pid = $presenter_id;
	$nv = $new_status;
	
	if ($stmt->execute()) {
	    return $new_status;
	} else {
	    return "Error";
	}

    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function sch_send_presenter_materials_request ($presenter_id) {

    $presenter = sch_get_presenter ($presenter_id);

    $link = sch_add_archive_link ($presenter_id);
    
    if ($link) {

	$body = "Thank you very much for your presentation at " . CONF_NAME . ".\n\n";

	$body .= "We would like to offer to host your presentation materials, including notes, transcript, slides, recordings and any other relevant materials.\n\n";

	if ($presenter['provide_transcript']) {
	    $body .= "You indicated at sign-up that you would be able to provide a transcript of your presentation. (Thank you!) If you are still willing, this is the place to do that!\n\n";
	} else {
	    $body .= "You indicated at sign-up that you would not be able to provide a transcript of your presentation. The option to upload materials and edit your presentation details is still available to you, but please do not feel any pressure to do so.\n\n";
	}

	$body .= "The page at the following link will allow you to confirm, edit or remove your presentation details, as well as upload any presentation materials that you would like us to host for you. There is no obligation to remain listed on this page.\n\n";

	$body .= SITE_URL . "archive/" . $link . "\n\n";

	$body .= "This link will expire after 72 hours, but you can always request a new link from the conference programme page:\n\n";

	$body .= SITE_URL . "programme/\n\n";

	$body .= "Any presentation materials submitted remain the property of the person who submits them. As much as is legally possible given the nature of this service, it is the policy of " . CONF_NAME . " that users retain complete creative and legal control of their own submitted material. We will not sell or monetize your presentation materials in any way, and you can remove them at any time.\n\n";

	$body .= "Best regards,\n\nThe organization team\n" . CONF_NAME . "\n\n";

	if (sch_email ($presenter['email'], "Please upload the presentation materials for '" . $presenter['title'] . "'", $body)) {
	    // Success
	    return TRUE;
	} else {
	    // Error sending email
	    return FALSE;
	}
	
    } else {
	// DB problem inserting link
	return FALSE;
    }

}

function sch_save_changes_to_archive ($link, $visible, $name, $pronouns, $handle, $email) {

    $linkdata = sch_get_archive_link ($link);

    if ($linkdata) {
	// Link is real

	try {
	    
	    $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	    $stmt = $dbh->prepare("UPDATE `presenters` SET `name` = :name, `pronouns` = :pronouns, `handle` = :handle, `email` = :email, `hidden_from_archive_presenter` = :hide WHERE `id` = :pid");

	    $stmt->bindParam(':name', $na);
	    $stmt->bindParam(':pronouns', $pr);
	    $stmt->bindParam(':handle', $ha);
	    $stmt->bindParam(':email', $em);
	    $stmt->bindParam(':hide', $hi);
	    $stmt->bindParam(':pid', $pid);

	    $na = $name;
	    $pr = $pronouns;
	    $ha = $handle;
	    $em = $email;
	    $hi = $visible;
	    $pid = $linkdata['presenter'];
	    
	    if ($stmt->execute()) {

		if (isset($_POST['mattypes'])) { // Some materials
		    
		    // Now combine the presentation materials arrays
		    $materials = array_map(
			null,
			$_POST['mattypes'], $_POST['mattitles'], $_POST['matcontent']
		    );

		    // Make an array of only the file materials type
		    $filemats = $materials;
		    foreach ($filemats as $fmkey => $fmelement) {
			if ($fmelement[0] != "file") {
			    unset($filemats[$fmkey]);
			}
		    }

		    // Get the list of all the files in the presenter directory
		    $presenter_dir = substr(md5($linkdata['presenter']), 0, 12);		
		    $allfiles = scandir(ABS_PATH . "archive/files/" . $presenter_dir . "/");

		    // Get rid of the `.` and `..`'s in the array
		    foreach ($allfiles as $afkey => $afelement) {
			if (substr($afelement, 0, 1) == ".") {
			    unset($allfiles[$afkey]);
			}
		    }

		    // Make a list of files that are not in the files materials
		    // These can be deleted
		    foreach ($allfiles as $fkey => $felement) {
			if (! in_array($felement, array_column($filemats, 2))) {
			    unlink(ABS_PATH . "archive/files/" . $presenter_dir . "/" . $felement);
			}
		    }

		    // Clear database for presenter
		    if (sch_remove_all_materials_for_presenter ($linkdata['presenter'])) {

			$sortorder = 1;
			// Insert into database
			foreach ($materials as $mat) {
			    if ($mat[2] != "") {
				sch_insert_archival_material ($linkdata['presenter'], $sortorder, $mat[0], $mat[1], $mat[2]);
			    }
			    $sortorder++;
			}

			return TRUE;
			
		    } else {
			return FALSE;
		    }
		    
		} else { // No materials

		    // Get the list of all the files in the presenter directory
		    $presenter_dir = substr(md5($linkdata['presenter']), 0, 12);		
		    $allfiles = scandir(ABS_PATH . "archive/files/" . $presenter_dir . "/");

		    // Get rid of the `.` and `..`'s in the array
		    foreach ($allfiles as $afkey => $afelement) {
			if (substr($afelement, 0, 1) == ".") {
			    unset($allfiles[$afkey]);
			}
		    }
		    
		    // Make a list of files that are not in the files materials
		    // These can be deleted
		    foreach ($allfiles as $fkey => $felement) {
			unlink(ABS_PATH . "archive/files/" . $presenter_dir . "/" . $felement);
		    }
		    
		    // Clear database for presenter
		    if (sch_remove_all_materials_for_presenter ($linkdata['presenter'])) {
			return TRUE;
		    } else {
			return FALSE;
		    }
		    
		}
		
	    } else {
		return FALSE;
	    }
	    
	}

	catch (PDOException $e) {

	    echo $e->getMessage();

	}
	
    } else {
	// Link not found
	return FALSE;
    }

    
}

function sch_delete_presenter_permanently ($link) {
    
    $linkdata = sch_get_archive_link ($link);

    if ($linkdata) {
	// Link is real
	
	try {
	    
	    $dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	    $stmt = $dbh->prepare("DELETE FROM `presenters` WHERE `id` = :pid; DELETE FROM `archive_link` WHERE `presenter` = :pid;");

	    $stmt->bindParam(':pid', $pid);

	    $pid = $linkdata['presenter'];
	    
	    if ($stmt->execute()) {
		return TRUE;
	    } else {
		return FALSE;
	    }
	    
	}

	catch (PDOException $e) {

	    echo $e->getMessage();

	}
	
    } else {
	// Link is not valid
	return FALSE;
    }
    
}

function sch_get_materials_for_presenter ($presenter_id) {
    
    try {
	
	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	$stmt = $dbh->prepare("SELECT * FROM `archive_materials` WHERE `presenter` = :pid ORDER BY `sortorder` ASC;");

	$stmt->bindParam(':pid', $pid);

	$pid = $presenter_id;
	
	if ($stmt->execute()) {
	    $result = $stmt->fetchAll();
	    return $result;
	} else {
	    return FALSE;
	}
	
    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function sch_get_all_materials () {
    
    try {
	
	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	$stmt = $dbh->prepare("SELECT * FROM `archive_materials` ORDER BY `sortorder` ASC;");
	
	if ($stmt->execute()) {
	    $result = $stmt->fetchAll();
	    return $result;
	} else {
	    return FALSE;
	}
	
    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function sch_remove_all_materials_for_presenter ($presenter_id) {
    
    try {
	
	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	$stmt = $dbh->prepare("DELETE FROM `archive_materials` WHERE `presenter` = :pid;");

	$stmt->bindParam(':pid', $pid);

	$pid = $presenter_id;
	
	if ($stmt->execute()) {
	    return TRUE;
	} else {
	    return FALSE;
	}
	
    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function sch_insert_archival_material ($presenter_id, $sortorder, $type, $title, $content) {
    
    try {
	
	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	$stmt = $dbh->prepare("INSERT INTO `archive_materials` (`sortorder`, `presenter`, `materials_type`, `materials_title`, `materials_content`) VALUES (:so, :pid, :typ, :tit, :con);");

	$stmt->bindParam(':so', $so);
	$stmt->bindParam(':pid', $pid);
	$stmt->bindParam(':typ', $typ);
	$stmt->bindParam(':tit', $tit);
	$stmt->bindParam(':con', $con);

	$so = $sortorder;
	$pid = $presenter_id;
	$typ = $type;
	$tit = $title;
	$con = $content;
	
	if ($stmt->execute()) {
	    return TRUE;
	} else {
	    return FALSE;
	}
	
    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

function sch_sanitize_text ($input, $longtext = FALSE) {

    $output = str_replace("<", "&lt;", $input);
    $output = str_replace(">", "&gt;", $output);
    $output = str_replace("\"", "&quot;", $output);

    if ($longtext) {
	$output = str_replace("\n", "<br>", $output);
    }

    return $output;
    
}

function sch_resend_archive_email ($email) {

    try {
	
	$dbh = new PDO('mysql:dbname=' . DB_NAME . ';host=' . DB_HOST, DB_USER, DB_PASS, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8mb4"));
	$stmt = $dbh->prepare("SELECT * FROM `presenters` WHERE `email` = :email LIMIT 1;");

	$stmt->bindParam(':email', $em);

	$em = $email;
	
	if ($stmt->execute()) {

	    $result = $stmt->fetchAll();

	    if (count ($result) > 0) {

		if (sch_send_presenter_materials_request($result[0]['id'])) {
		    return TRUE;
		} else {
		    return FALSE;
		}
		
	    } else {
		return TRUE;
	    }
	    
	} else {
	    return FALSE;
	}
	
    }

    catch (PDOException $e) {

	echo $e->getMessage();

    }
    
}

?>
