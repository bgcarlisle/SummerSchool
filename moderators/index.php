<?php

include_once ("../config.php");

include (ABS_PATH . "header.php");

if (time() >= strtotime(PRESENT_SIGNUP_START) & time() < strtotime(PRESENT_SIGNUP_END) + 86400) {
    $presenter_form_avail = TRUE;
} else {
    $presenter_form_avail = FALSE;
}

if (time() >= strtotime(MOD_SIGNUP_START) & time() < strtotime(MOD_SIGNUP_END) + 86400) {
    $mod_form_avail = TRUE;
} else {
    $mod_form_avail = FALSE;
}

if (time() <= strtotime(CONF_END)) {
    $partic_form_avail = TRUE;
} else {
    $partic_form_avail = FALSE;
}

?>
<div class="container">
    <div class="row">
	<div class="col-md-12">
	    <h3>Information for moderators</h3>

	    <p>We expect moderators to:</p>

	    <ul>
		<li>At the beginning of the session announce the presenter by their name, pronouns (if provided), handle (@username@instance.name), and title of the presentation.</li>
		<li>Tell the audience the rules: No vocal interruption of the presenter, hold all questions and comments until the end, use the text chat, turn off your camera, turn off your microphone until the end of the talk.</li>
		<li>If the presenter wants to record the talk, ask all audience members if they consent with the recording, and if not, don't.</li>
		<li>During the presentation, use the text chat as time keeping for the presenter. Announce 10, 5, and 0-minute marks, and wrap up the presentation.</li>
		<li>Because the talks are relatively short, we're operating on a one strike rule. If someone makes a comment that is offensive or out of line, mute them or kick them out of the session.</li>
		<li>We won't tolerate any queerphobia, racism, misogyny, harassment, etc.</li>
		<li>Prepare beforehand to use the moderation tools available on the video conferencing platform being used.</li>
		<li>Determine whether captioning will be available on the video conference platform to be used.</li>
		<li>Provide participants and the presenter with a link to a feedback form so the organizers can keep track of how the conference is going and improve on our efforts.</li>
	    </ul>

	    <p>Either the moderator or the presenter must be able to provide a venue (publicly available link to a video conference room) no later than 24 hours before the scheduled beginning of the presentation. This will be coordinated by the organizers.</p>

	    <?php if ($presenter_form_avail) { ?>
		<p>The <a href="<?php echo SITE_URL; ?>presenters-signup/">signup form for presenters</a> is open from <?php echo PRESENT_SIGNUP_START; ?> to <?php echo PRESENT_SIGNUP_END; ?>.</p>
	    <?php } ?>
	    
	    <?php if ($mod_form_avail) { ?>
		<p>The <a href="<?php echo SITE_URL; ?>moderators-signup/">signup form for moderators</a> is open from <?php echo MOD_SIGNUP_START; ?> to <?php echo MOD_SIGNUP_END; ?>.</p>
	    <?php } ?>
	    
	    <?php if ($partic_form_avail) { ?>
		<p>The <a href="<?php echo SITE_URL; ?>programme/">signup form for participants</a> is open starting <?php echo PARTIC_SIGNUP_START; ?> and you can sign up for any presentation until the presentation ends.</p>
	    <?php } ?>
	    
	</div>
    </div>
</div>
<?php

include (ABS_PATH . "footer.php");

?>
