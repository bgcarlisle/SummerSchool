<div class="container">
    <div class="row">
	<div class="col-md-12">
	    <h3>Contact us</h3>

	    <div id="contact_form">

		<div class="form-group" style="margin-bottom: 10px;">
		    <label for="contact_email">Your email address</label>
		    <input type="email" class="form-control" id="contact_email" aria-describedby="emailHelp" placeholder="Enter email">
		    <small id="emailHelp" class="form-text text-muted">This is optional, but if you don't give us a way to contact you, we won't be able to reply</small>
		</div>
		
		<div class="form-group" style="margin-bottom: 20px;">
		    <label for="contact_text">Your message to the organizers</label>
		    <textarea class="form-control" id="contact_text" rows="5"></textarea>
		</div>

		<button type="button" id="send-contact-form" class="btn btn-primary btn-lg">Send message</button>

	    </div>
	    
	</div>
    </div>
</div>
