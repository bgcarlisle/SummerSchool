<?php

include_once ("../config.php");

include (ABS_PATH . "header.php");

?>
<div class="container">
    <div class="row">
	<div class="col-md-12">
	    <h3>Accessibility</h3>
	    <ul>
		<li>Presentation transcripts: Every presenter will be prompted to provide a transcript of their talk that will be published afterward to aid in accessibility. This is not mandatory, because we do not want to exclude presenters who are unable to do so.</li>
		<li>Live captioning: Thanks to the generousness of <a href="https://cathode.church/@s0">@s0@cathode.church</a>, we will be providing Jitsi video conferencing rooms with live captioning to aid in accessibility.</li>
	    </ul>
	    <p>We would love to make this conference more accessible. <a href="<?php echo SITE_URL; ?>contact/">Please contact us</a> if we are overlooking a way to improve in this regard.</p>
	</div>
    </div>
</div>
<?php

include (ABS_PATH . "footer.php");

?>
