<?php $presenters = sch_get_presenters ("confirmed"); ?>
<div class="container">
    <div class="row">
	<div class="col-md-12">
	    
	    <h3>Moderator signup</h3>
	    <p>Use the following form to sign up to be a moderator for a presentation at <?php echo CONF_NAME; ?>. This form will be available until 23:59 UTC on <?php echo MOD_SIGNUP_END;  ?>.</p>
	    <p><a href="<?php echo SITE_URL; ?>moderators/">More info on moderating</a></p>

	    <hr>

	    <div id="moderator_form">

		<h4>About you</h4>

		<div class="form-group">
		    <label for="mod_name">Your name</label>
		    <input type="text" class="form-control" id="mod_name" placeholder="E.g. Penguin">
		    <small id="mod_name_help" class="form-text text-muted">
			This doesn't need to be your legal name; use the name that you would like us to use to refer to you.
		    </small>
		</div>
		
		<div class="form-group">
		    <label for="mod_pronouns">Your pronouns</label>
		    <input type="text" class="form-control" id="mod_pronouns" placeholder="E.g. they/them">
		    <small id="mod_pronouns_help" class="form-text text-muted">
			Optional
		    </small>
		</div>
		
		<div class="form-group">
		    <label for="mod_handle">Your handle on the Fediverse</label>
		    <input type="text" class="form-control" id="mod_handle" placeholder="@SummerSchool@scholar.social">
		    <small id="mod_handle_help" class="form-text text-muted">
			Use the handle that you would like to be credited for moderation
		    </small>
		</div>
		
		<div class="form-group">
		    <label for="mod_email">Your email</label>
		    <input type="email" class="form-control" id="mod_email" placeholder="yourname@domain.egg">
		    <small id="mod_email_help" class="form-text text-muted">
			Use an email address where you can be reached to confirm your availability to moderate when you have been matched with a presenter; if you also signed up to present, it's recommended to use the same email as for your presentation so that we can automatically check that we don't accidentally ask you to moderate your own presentation
		    </small>
		</div>

		<hr>

		<h4>Your availability</h4>
		
		<div class="form-group mb-3">
		    <label for="mod_number">What is the maximum number of presentations you would be willing to moderate?</label>
		    <select class="form-control form-control-sm" id="mod_number">
			<option value="1" selected>1</option>
			<?php

			$mod_num = 1;
			
			while ($mod_num < count($presenters)) {
			    $mod_num++;
			    echo '<option value=' . $mod_num . '>' . $mod_num . '</option>';
			    
			}
			
			?>
		    </select>
		    <small id="mod_handle_number" class="form-text text-muted">
			Regardless of the number of presentations you check below, you will only be asked to moderate this many at most
		    </small>
		</div>

		<p>Please check <em>every</em> presentation time that you would be available and willing to moderate. You will not be matched with more presentations than the number you offered to moderate above, regardless of how many are checked below.</p>

		<p>Please do not offer to moderate your own presentation.</p>

		<p>I am available and willing to moderate at the following presentation times:</p>

		<?php
		
		// Read the time zone data into memory
		$tzfile = fopen(ABS_PATH . "timezones.csv", "r");
		$timezones = [];
		while (! feof($tzfile)) {
		    $timezones[] = fgetcsv($tzfile);
		}
		fclose($tzfile);

		?>
		<div class="form-group mb-3">
		    <label for="mod_signup_tz_selector">Display times in another time zone</label>
		    <select class="form-control form-control-sm" id="mod_signup_tz_selector">
			<option value="0">UTC</option>
			<?php

			// The variable $tz[2] in the loop below means that we take
			// daylight savings time; switch to $tz[1] for standard time
			foreach ($timezones as $tz) {
			    if ($tz[0] != "timezone" & $tz[0] != "UTC") {
				echo '<option value="' . $tz[2] . '">';
				echo $tz[0];
				echo '</option>';
			    }
			}
			
			?>
		    </select>
		</div>

		<?php foreach ($presenters as $presenter) { ?>
		    <?php

		    $utc_time = date("Y-m-d (D) H:i", strtotime(CONF_START) + ($presenter['confirmed_slot'] - 1) * 60 * 60) . " UTC";

		    if ($presenter['able_to_host'] == 1) {
			$able_to_host_class = " able_to_host";
		    } else {
			$able_to_host_class = " not_able_to_host";
		    }
		    
		    ?>
		    <div class="form-check<?php echo $able_to_host_class; ?>">
			<input class="form-check-input mod-signup-time-checkbox" type="checkbox" value="TRUE" data-slot="<?php echo $presenter['id']; ?>" id="mod_presenter_<?php echo $presenter['id']; ?>">
			<label class="form-check-label" for="mod_presenter_<?php echo $presenter['id']; ?>">
			    <span class="signup_time" data-slot="<?php echo $presenter['confirmed_slot']; ?>"><?php echo $utc_time; ?></span> (<?php echo $presenter['name']; ?>)
			</label>
		    </div>
		<?php } ?>
		<input type="hidden" id="checked_presentations" value="">
		
		<hr>

		<div class="form-check mb-3">
		    <input class="form-check-input" type="checkbox" value="TRUE" id="mod_policy">
		    <label class="form-check-label" for="mod_policy">
			I have read, understood and agree to the <a href="<?php echo SITE_URL; ?>moderators/">moderation policies</a> for <?php echo CONF_NAME; ?>
		    </label>
		</div>
		
		<p class="mb-3">We will contact you by email to confirm your moderation duties as soon as the moderation schedule is finalized.</p>

		<button id="mod-signup-button" class="btn btn-lg btn-primary" disabled>Sign up to moderate</button>
	    </div>

	</div>
    </div>
</div>
