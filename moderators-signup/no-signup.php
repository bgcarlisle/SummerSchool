<div class="container">
    <div class="row">
	<div class="col-md-12">
	    
	    <h3>Moderator signup</h3>
	    <p>This form is only available between <?php echo MOD_SIGNUP_START; ?> and <?php echo MOD_SIGNUP_END; ?> (inclusive).</p>
	    <p><a href="<?php echo SITE_URL; ?>moderators/">More info on moderating</a></p>

	</div>
    </div>
</div>
