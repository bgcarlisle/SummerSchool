<?php

include_once ("../config.php");

include (ABS_PATH . "header.php");

$confirm = sch_get_presenter_confirmation_by_link ($_GET['link']);

$presenter = sch_get_presenter ($confirm['presenter']);

$availabilities = sch_get_presenter_availabilities ($confirm['presenter']);

$utc_time = date("Y-m-d (D) H:i", strtotime(CONF_START) + ($presenter['assigned_slot'] - 1) * 60 * 60) . " UTC";

?>

<?php if ($confirm) { ?>
    <!-- Confirmation link found -->
    <div class="container">
	<div class="row">
	    <div class="col-md-12">
		<h3>Confirm your presentation</h3>

		<p>Thank you very much for submitting a presentation to <?php echo CONF_NAME; ?>! We just need to confirm the details of your presentation before we assemble the programme and recruit moderators.</p>

		<hr>

		<div id="presentation-details-container">
		
		    <h4>Presentation time</h4>

		    <p>We are pleased to offer you the following presentation time:</p>

		    <input type="hidden" id="confirm_assigned_slot" value="<?php echo $presenter['assigned_slot']; ?>">
		    <p><span class="assigned_slot_text"><?php echo $utc_time; ?></span> <a href="<?php echo SITE_URL; ?>presenters-signup/calendar.php?link=<?php echo $_GET['link']; ?>">Download event as an .ics file</a></p>
		    
		    <?php
		    
		    // Read the time zone data into memory
		    $tzfile = fopen(ABS_PATH . "timezones.csv", "r");
		    $timezones = [];
		    while (! feof($tzfile)) {
			$timezones[] = fgetcsv($tzfile);
		    }
		    fclose($tzfile);

		    ?>
		    <div class="form-group mb-3">
			<label for="presenter_confirm_tz_selector">Display presentation time in another time zone</label>
			<select class="form-control form-control-sm" id="presenter_confirm_tz_selector">
			    <option value="0">UTC</option>
			    <?php

			    // The variable $tz[2] in the loop below means that we take
			    // daylight savings time; switch to $tz[1] for standard time
			    foreach ($timezones as $tz) {
				if ($tz[0] != "timezone" & $tz[0] != "UTC") {
				    echo '<option value="' . $tz[2] . '">';
				    echo $tz[0];
				    echo '</option>';
				}
			    }
			    
			    ?>
			</select>
		    </div>

		    <div class="form-check">
			<input class="form-check-input confirm-presentation-time" type="radio" name="confirm-presentation-time" id="confirm-presentation-time-yes" value="yes">
			<label class="form-check-label" for="confirm-presentation-time-yes">
			    Yes, I am able to present at this time
			</label>
		    </div>
		    <div class="form-check">
			<input class="form-check-input confirm-presentation-time" type="radio" name="confirm-presentation-time" id="confirm-presentation-time-no" value="no">
			<label class="form-check-label" for="confirm-presentation-time-no">
			    No, I am not able to present at this time
			</label>
		    </div>
		    <div class="form-check mb-3">
			<input class="form-check-input confirm-presentation-time" type="radio" name="confirm-presentation-time" id="confirm-presentation-time-never" value="never">
			<label class="form-check-label" for="confirm-presentation-time-never">
			    No, I am not able to present during this conference; please withdraw my presentation
			</label>
		    </div>

		    <div id="confirm-presentation-cant-present-container" style="display: none;">
			<p>Please select all the times you are available to present from the grid below and we'll try to reschedule your presentation</p>
			<div id="availability_table">
			    <?php include (ABS_PATH . "presenters-signup/availability-table.php"); ?>
			</div>
			<input type="hidden" id="checked_slots" value="<?php echo implode(",", array_column($availabilities, 'slot')); ?>">
		    </div>

		    <div id="confirm-details-and-presentation-container">

			<hr>

			<h4>Your details</h4>
			
			<p>Please carefully review your presentation details for accuracy. They will appear on the conference programme exactly as they appear here. (Of course, if something needs to be changed, you can always contact us and we will do so manually, but you will not be prompted to make edits to your presentation details again.)</p>

			<div class="form-group">
			    <label for="presenter_name">Your name</label>
			    <input type="text" class="form-control" id="presenter_name" placeholder="E.g. Otter" value="<?php echo str_replace("\"", "&quot;", $presenter['name']); ?>">
			    <small id="presenter_name_help" class="form-text text-muted">
				This doesn't need to be your legal name; use the name that you would like us to use to refer to you.
			    </small>
			</div>
			
			<div class="form-group">
			    <label for="presenter_pronouns">Your pronouns</label>
			    <input type="text" class="form-control" id="presenter_pronouns" placeholder="E.g. they/them" value="<?php echo str_replace("\"", "&quot;", $presenter['pronouns']); ?>">
			    <small id="presenter_pronouns_help" class="form-text text-muted">
				Optional
			    </small>
			</div>
			
			<div class="form-group">
			    <label for="presenter_handle">Your handle on the Fediverse</label>
			    <input type="text" class="form-control" id="presenter_handle" placeholder="@SummerSchool@scholar.social" value="<?php echo $presenter['handle']; ?>">
			    <small id="presenter_handle_help" class="form-text text-muted">
				Use the handle that you would like to be credited for your presentation
			    </small>
			</div>

			<hr>

			<h4>Your presentation</h4>

			<div class="form-group">
			    <label for="presenter_title">Title</label>
			    <input type="text" class="form-control" id="presenter_title" placeholder="Baking the best apple crumble to celebrate #MastodonCrumbles" value="<?php echo str_replace("\"", "&quot;", $presenter['title']); ?>">
			    <small id="presenter_title_help" class="form-text text-muted">
				A short title for your presentation
			    </small>
			</div>

			<div class="form-group">
			    <label for="presenter_abstract">Abstract</label>
			    <textarea class="form-control" id="presenter_abstract" rows="8"><?php echo $presenter['abstract']; ?></textarea>
			    <small id="presenter_title_help" class="form-text text-muted">
				Aim for 500 words or less
			    </small>
			</div>

			<div class="form-check">
			    <input class="form-check-input" type="checkbox" value="TRUE" id="confirm_presenter_host"<?php if ($presenter['able_to_host'] == 1) echo " checked"; ?>>
			    <label class="form-check-label" for="confirm_presenter_host">
				I prefer to host my own presentation by providing a publicly available video conference room
			    </label>
			</div>

			<div class="mb-3" id="hosting_details_container"<?php if ($presenter['able_to_host'] != 1) echo 'style="display: none;"'; ?>>
			    <div class="alert alert-success" role="alert">
				Thanks for agreeing to provide a video conferencing room link! Please prepare one now and enter it below.
			    </div>
			    <div class="form-group">
				<label for="presenter_hosting_details">Link to video conferencing room</label>
				<input type="text" class="form-control" id="presenter_hosting_details" placeholder="E.g. a Zoom or Jitsi link" value="<?php echo $presenter['hosting_details']; ?>">
				<small id="presenter_hosting_details_help">
				    This link will not be shared with participants until the day before your presentation
				</small>
			    </div>
			</div>

			<div class="form-check">
			    <input class="form-check-input" type="checkbox" value="TRUE" id="presenter_recording"<?php if ($presenter['consent_to_record'] == 1) echo " checked"; ?>>
			    <label class="form-check-label" for="presenter_recording">
				I would like to have my presentation recorded
			    </label>
			</div>

			<div class="form-check">
			    <input class="form-check-input" type="checkbox" value="TRUE" id="provide_transcript"<?php if ($presenter['provide_transcript'] == 1) echo " checked"; ?>>
			    <label class="form-check-label" for="provide_transcript">
				I am able to provide a transcript of my presentation after the fact to aid in accessibility
			    </label>
			</div>
			
		    </div>

		    <hr>
		    
		    <button id="presenter-confirm-button" class="btn btn-primary btn-lg" data-link="<?php echo $confirm['link']; ?>" disabled>
			Save details and send response
		    </button>

		</div>
		
	    </div>
	</div>
    </div>
<?php } else { ?>
    <!-- Confirmation link not found -->
    <div class="container">
	<div class="row">
	    <div class="col-md-12">
		<div class="alert alert-danger" role="alert">
		    Error opening confirmation link: invalid, expired, or database connection issue
		</div>
		
	    </div>
	</div>
    </div>
<?php } ?>

<?php

include (ABS_PATH . "footer.php");

?>
