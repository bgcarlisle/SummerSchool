<?php

include_once ("../config.php");

if (time() >= strtotime(PRESENT_SIGNUP_START) & time() < strtotime(PRESENT_SIGNUP_END) + 86400) {

    sch_save_presenter (
	$_POST['name'],
	$_POST['pronouns'],
	$_POST['handle'],
	$_POST['email'],
	$_POST['title'],
	$_POST['abstract'],
	$_POST['host'],
	$_POST['recording'],
	$_POST['transcript'],
	$_POST['availability']
    );
    
} else {
    echo "<p>This form is not currently accepting submissions.</p>";
}

?>
