<?php

include_once ("../config.php");

include (ABS_PATH . "header.php");

if (time() >= strtotime(PRESENT_SIGNUP_START) & time() < strtotime(PRESENT_SIGNUP_END) + 86400) {
    include (ABS_PATH . "presenters-signup/signup.php");
} else {
    include (ABS_PATH . "presenters-signup/no-signup.php");
}


include (ABS_PATH . "footer.php");

?>
