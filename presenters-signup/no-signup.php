<div class="container">
    <div class="row">
	<div class="col-md-12">
	    
	    <h3>Presenter signup</h3>
	    <p>This form is only available between <?php echo PRESENT_SIGNUP_START; ?> and <?php echo PRESENT_SIGNUP_END; ?> (inclusive).</p>
	    <p><a href="<?php echo SITE_URL; ?>presenters/">More info on presenting</a></p>

	</div>
    </div>
</div>
