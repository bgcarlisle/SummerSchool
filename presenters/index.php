<?php

include_once ("../config.php");

include (ABS_PATH . "header.php");

if (time() >= strtotime(PRESENT_SIGNUP_START) & time() < strtotime(PRESENT_SIGNUP_END) + 86400) {
    $presenter_form_avail = TRUE;
} else {
    $presenter_form_avail = FALSE;
}

if (time() >= strtotime(MOD_SIGNUP_START) & time() < strtotime(MOD_SIGNUP_END) + 86400) {
    $mod_form_avail = TRUE;
} else {
    $mod_form_avail = FALSE;
}

if (time() <= strtotime(CONF_END)) {
    $partic_form_avail = TRUE;
} else {
    $partic_form_avail = FALSE;
}

?>
<div class="container">
    <div class="row">
	<div class="col-md-12">
	    <h3>Information for presenters</h3>

	    <p>Scholar Social is hosting <?php echo CONF_NAME; ?>, an informal online conference covering a variety of topics, with presentations occurring between <?php echo CONF_START; ?> and <?php echo CONF_END; ?>. Our goals are to:</p>

	    <ul>
		<li>provide scholars with a low-stakes venue for talking about our work and receiving feedback</li>
		<li>provide all of the Fediverse knowledge and insight from people of multiple backgrounds, and</li>
		<li>carry forward the ethics of Scholar Social regarding freedom of knowledge, accessibility and mutual support.</li>
	    </ul>

	    <p>Presentation spots are open to researchers, writers, formal academics, teachers, and anyone with expertise they're willing to share. You <strong>do not</strong> have to be on scholar.social to do this!</p>

	    <p>This year we are revamping it a bit. If you’d like to submit, consider submitting a topic that is not overly technical (i.e. not me talking about how to do a Bayesian analysis, but more like impact of Stats on public perception of political/social topics). </p>

	    <p>Each talk will be about 10-15 minutes with time for questions and discussion afterwards, coming to a total of about half an hour to 45 minutes. You do not need formal training, advanced degrees or academic affiliation to present. If you have some expertise or knowledge that you would like to share, we would love to provide a venue for you to do so.</p>

	    <p>We will reject your application if your proposed presentation falls into one of the following categories:</p>

	    <ul>
		<li>Overtly racist, sexist, homophobic, transphobic, able-ist, etc.</li>
		<li>Pro-fascist, or fascist-adjacent</li>
		<li>Harmful pseudo-science, e.g. anti-vaxxers, flat-earthers, "race science", etc.</li>
		<li>Scams, e.g. multi-level marketing, pro-cryptocurrency/NFT presentations, etc.</li>
		<li>Presentations in support of, or given by law enforcement</li>
		<li>Fediverse "meta" (research or commentary on the Fediverse itself)</li>
	    </ul>

	    <p>We would love to highlight new voices who have not presented previously, And we want grad students and autodidacts to apply! We are limiting the Conference to 20 presentations so the earlier the submission the better.</p>

	    <p>In order to keep the conference at a manageable size, we will accept a maximum of 20 presentations this year. If  more than 20 applications are received, we will give priority 1. to those who have not presented in previous years and 2. in a manner that tries to maintain a diversity of presenters from different time zones and locales. In the case that we have more than 20 presenters, all of whom are new to Solstice School, and where it would be difficult to give priority based on time zone/location diversity, we will assign places at the conference by random lottery.</p>

	    <p>Presentations for this conference are (at least for now) English-only. We do not have the resources to provide moderation in other languages, but we will consider this for future iterations.</p>

	    <?php if ($presenter_form_avail) { ?>
		<p>The <a href="<?php echo SITE_URL; ?>presenters-signup/">signup form for presenters</a> is open from <?php echo PRESENT_SIGNUP_START; ?> to <?php echo PRESENT_SIGNUP_END; ?>.</p>
	    <?php } ?>

	    <?php if ($mod_form_avail) { ?>
		<p>The <a href="<?php echo SITE_URL; ?>moderators-signup/">signup form for moderators</a> is open from <?php echo MOD_SIGNUP_START; ?> to <?php echo MOD_SIGNUP_END; ?>.</p>
	    <?php } ?>
	    
	    <?php if ($partic_form_avail) { ?>
		<p>The <a href="<?php echo SITE_URL; ?>programme/">signup form for participants</a> is open starting <?php echo PARTIC_SIGNUP_START; ?> and you can sign up for any presentation until the presentation ends.</p>
	    <?php } ?>
	    
	</div>
    </div>
</div>
<?php

include (ABS_PATH . "footer.php");

?>
